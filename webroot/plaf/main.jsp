﻿<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<html lang="zh-CN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,user-scalable=no, initial-scale=1">
<meta name="format-detection" content="telephone=no" />
<meta content="紫云读书社" http-equiv="keywords">
<meta name="description" content="广外图书馆紫云读书社">
<title>新生“闯”馆小程序</title>
<link rel="shortcut icon" href="plaf/images/favicon.ico">
<link rel="stylesheet" href="plaf/css/index.css" type="text/css">
<link rel="stylesheet" href="plaf/css/zy.css" type="text/css">
<link rel="stylesheet" href="plaf/css/bootstrap/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="plaf/css/main.css" type="text/css">
<script type="text/javascript" src="plaf/js/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="plaf/js/md5.js"></script>
<script type="text/javascript" src="plaf/js/main.js"></script>

<!-- <link rel="stylesheet" href="plaf/css/swiper.min.css" type="text/css">
<script type="text/javascript" src="plaf/js/swiper.min.js"></script> -->

<script type="text/javascript">
function getToday() {
    var now = new Date();
    var year = now.getFullYear(); //得到年份
    var month = now.getMonth();//得到月份
    var date = now.getDate();//得到日期
    month = month + 1;
    if (month < 10) {month = "0" + month;}
    if (date < 10) {date = "0" + date;}
    var today=year + month + date;
    return today;
}
 $().ready(function() {
	 var today=getToday();
		if ('${user.dailyAttendance}' == '0' || '${user.dailyAttendance}' == today) {//每日打卡
			$("#daily_a").html('已签到');
			$('#daily_a').attr("return", "false;");
			$("#daily_a").attr('class', 'fil-btn butns location');//gerzx_a2
		} else {
			$("#daily_a").html('每日签到');
			$('#daily_a').attr("onclick", "daily();");
			$("#daily_a").attr('class', 'fil-btn butns location');//gerzx_a1
		}

		if ('${user.subscribe}' == "0" || '${user.subscribe}' == '00') {//关注微信公众号
			$("#subscribe_btn").html('已完成');
			$('#subscribe_btn').attr("onclick", "toSubscribe('${user.userId}',0)");
			$("#subscribe_btn").attr('class', 'fil-btn-finish butns_f');
		} else {
			$("#subscribe_btn").html('去完成');
			$('#subscribe_btn').attr("onclick", "toSubscribe('${user.userId}',1);");
			$("#subscribe_btn").attr('class', 'fil-btn butns');
		}
		
		if ('${user.readStatus}' == '0' || '${user.readStatus}' == '00') {//入馆常识学习
			$("#general_btn").html('已完成');
			//$('#general_btn').attr("onclick", "cGeneralLearn();");
			$("#general_btn").attr('class', 'fil-btn-finish butns_f');
		} else {
			$("#general_btn").html('去完成');
			//$('#general_btn').attr("onclick", "cGeneralLearn();");
			$("#general_btn").attr('class', 'fil-btn butns');
		}
		
		if ('${user.generalLearn}' == '0' || '${user.generalLearn}' == '00') {//入馆答题学习
			$("#answer_btn").html('已完成');
			//$('#answer_btn').attr("onclick", "toQuestion();");
			$("#answer_btn").attr('class', 'fil-btn-finish butns_f');
		} else {
			$("#answer_btn").html('去完成');
			//$('#answer_btn').attr("onclick", "toQuestion();");
			$("#answer_btn").attr('class', 'fil-btn butns');
		}
		
		if ('${user.readerRegister}' == '0' || '${user.readerRegister}' == '00') {//读者登记
			$("#joinregister_btn").html('已完成');
			//$('#joinregister_btn').attr("onclick", "cJoinRegister('${user.account}');");
			$("#joinregister_btn").attr('class', 'fil-btn-finish butns_f');
		} else {
			$("#joinregister_btn").html('去完成');
			//$('#joinregister_btn').attr("onclick", "cJoinRegister('${user.account}');");
			$("#joinregister_btn").attr('class', 'fil-btn butns');
		}

		if ('${user.enterLibrary}' == '0' || '${user.enterLibrary}' == '00') {//第一次入馆打卡
			$("#facecard_btn").html('已完成');
			$('#facecard_btn').attr("return", "false");
			$("#facecard_btn").attr('class', 'fil-btn-finish butns_f');
		} else {
			$("#facecard_btn").html('去完成');
			$('#facecard_btn').attr("onclick", "cEnterLibrary()");
			$("#facecard_btn").attr('class', 'fil-btn butns');
		}

		if ('${user.loanBook}' == '0' || '${user.loanBook}' == '00') {//自助借书
			$("#loanbook_btn").html('已完成');
			$('#loanbook_btn').attr("return", "false");
			$("#loanbook_btn").attr('class', 'fil-btn-finish butns_f');
		} else {
			$("#loanbook_btn").html('去完成');
			$('#loanbook_btn').attr("onclick", "cLoanBook();");
			$("#loanbook_btn").attr('class', 'fil-btn butns');
		}

		if ('${user.returnBook}' == '0' || '${user.returnBook}' == '00') {//自助还书
			$("#returnbook_btn").html('已完成');
			$('#returnbook_btn').attr("return", "false");
			$("#returnbook_btn").attr('class', 'fil-btn-finish butns_f');
		} else {
			$("#returnbook_btn").html('去完成');
			$('#returnbook_btn').attr("onclick", "cReturnBook();");
			$("#returnbook_btn").attr('class', 'fil-btn butns');
		}
		
		if ('${user.downloadJournal}' == '0' || '${user.downloadJournal}' == '00') {//下载文章
			$("#downloadqk_btn").html('已完成');
			$('#downloadqk_btn').attr("return", "false");
			$("#downloadqk_btn").attr('class', 'fil-btn-finish butns_f');
		} else {
			$("#downloadqk_btn").html('去完成');
			$('#downloadqk_btn').attr("onclick", "cDownloadqk();");
			$("#downloadqk_btn").attr('class', 'fil-btn butns');
		}
		
		if ('${user.activities}' == '0' || '${user.activities}' == '00') {//活动
			$("#activity_btn").html('已完成');
			//$('#activity_btn').attr("return", "false");
			$('#activity_btn').attr("onclick", "toActivity('${user.account}');");
			$("#activity_btn").attr('class', 'fil-btn-finish butns_f');
		} else {
			$("#activity_btn").html('去完成');
			$('#activity_btn').attr("onclick", "toActivity('${user.account}');");
			$("#activity_btn").attr('class', 'fil-btn butns');
		}
		
		if ('${user.questionnaire}' == '0' || '${user.questionnaire}' == '00') {//问卷调查
			$("#questionnaire_btn").html('已完成');
			//$('#questionnaire_btn').attr("return", "false");
			$('#questionnaire_btn').attr("onclick", "toQuestionnaire('${user.account}');");
			$("#questionnaire_btn").attr('class', 'fil-btn-finish butns_f');
		} else {
			$("#questionnaire_btn").html('去完成');
			$('#questionnaire_btn').attr("onclick", "toQuestionnaire('${user.account}');");
			$("#questionnaire_btn").attr('class', 'fil-btn butns');
		}
}); 
function cEnterLibrary() {
	$("#facecard_p").html('请去刷卡或刷脸入馆！');
}
function cLoanBook() {
	$("#loanbook_p").html('请通过自助借还设备借一次书！');
}
function cReturnBook() {
	$("#returnbook_p").html('请通过自助借还设备还一次书！');
}
function cDownloadqk() {
	$("#downloadqk_p").html('请通过校外访问下载一篇文章！');
}
function cJoinRegister2(uid) {
	window.location.href = 'toUserInfo.htm';
}
//滑动头部透明（针对首页头部用）
window.onscroll = function() {
	var autoheight = document.body.scrollTop || document.documentElement.scrollTop;
	if (autoheight > 20) {
		$(".toub_beij").css("position", "fixed")
	} else {
		$(".toub_beij").css("position", "relative")
	}
}
</script>
</head>

<body style="height: 100%;background: aliceblue; width: 95%;" class="container">
	<!-- 头部 -->
	<div class="gerzx_toub">
		<input type="hidden" id="userId" value="${user.userId}"> 
		<input type="hidden" id="account" value="${user.account}">
		<!-- <a href="javascript:void();" class="gerzx_a1" id="daily_a0" onclick="daily();">每日签到</a> -->
		<div class="gerzx_tx">
			<div class="toux_k">
				<a href="#"><img src="plaf/images/a9.png"></a>
			</div>
			<div class="mignz_k">
				<p><i>${user.account}</i><!-- <a href="logout">[安全退出]</a> --></p>
				<span>积分:&nbsp;</span><label id="score_label">${user.score}</label>
			</div>
			<button class="fil-btn butns location" style="width: 54%;height: 45%;font-size: 0.2rem;margin-left: 167%;" id="daily_a" onclick="daily();">每日签到</button>
			
		</div>
		<div class="fonts">
			<div class="title_tips"><font class="fontset">读者完成1-5项开通权限，6-11项累计加分，参与和完成问卷调查加50分，其他每项10分</font></div>
		</div>
	</div>
	<!-- 内容 -->
	<div class="center_">
	<!--关注图书馆公众号 -->
		<div class="patre">
			<div class="row">
				<div class="topfont col-sm-3">
					<div class="title">1.关注图书馆公众号</div>
					<div class="fontqw">读者关注广外图书馆微信公众号和紫云读书社微信公众号</div>
				</div>
				<img class="imga col-sm-3" src="plaf/images/img_1.png">
			 </div>
			 <hr class="lines">
			 <div class="row lints">
				<div class="right">完成可获取积分 ：10</div>
				<button class="fil-btn butns" id="subscribe_btn" onclick="toSubscribe('${user.userId}',1)">去完成</button>
			</div>
			<div class="border_line"> &nbsp;</div> 
		</div>
		<!-- 入馆常识 -->
		<div class="patre">
			<div class="row">
				<div class="topfont col-sm-3">
					<div class="title">2.入馆常识</div>
					<div class="fontqw">观看小视频，学习图书馆常用服务</div>
				</div>
				<img class="imga col-sm-3" src="plaf/images/img_2.png">
			 </div>
			 <hr class="lines">
			 <div class="row lints">
				<div class="right">完成可获取积分 ：10</div>
				<button class="fil-btn butns" id="general_btn" onclick='cGeneralLearn()'>去完成</button>
			</div>
			<div class="border_line"> &nbsp;</div> 
		</div>
		<!-- 新生答题 -->
		<div class="patre">
			<div class="row">
				<div class="topfont col-sm-3">
					<div class="title">3.新生答题</div>
					<div class="fontqw">答题分数满80分即可通关 (每小题5分,共100分)</div>
				</div>
				<img class="imga col-sm-3" src="plaf/images/img_3.png">
			 </div>
			 <hr class="lines">
			 <div class="row lints">
				<div class="right">完成可获取积分 ：10</div>
				<button class="fil-btn butns" id="answer_btn" onclick='toQuestion()'>去完成</button>
			</div>
			<div class="border_line"> &nbsp;</div> 
		</div>
		<!-- 读者登记 -->
		<div class="patre">
			<div class="row">
				<div class="topfont col-sm-3">
					<div class="title">4.读者登记</div>
					<div class="fontqw">读者完善个人手机、邮箱等信息，以便接收图书馆借还书等短信通知，享受“帮你找书”服务</div>
				</div>
				<img class="imga col-sm-3" src="plaf/images/img_4.png">
			 </div>
			 <hr class="lines">
			 <div class="row lints">
				<div class="right">完成可获取积分 ：10</div>
				<button class="fil-btn butns" id="joinregister_btn" onclick="cJoinRegister2('${user.account}');">去完成</button>
			</div>
			<div class="border_line"> &nbsp;</div> 
		</div>
		<!-- 入馆打卡 -->
		<div class="patre">
			<div class="row">
				<div class="topfont col-sm-3">
					<div class="title">5.入馆打卡</div>
					<div class="fontqw">读者刷卡或人脸识别，双轨道任意方式入馆一次。</div>
				</div>
				<img class="imga col-sm-3" src="plaf/images/img_1.png">
			 </div>
			 <hr class="lines">
			 <div class="row lints">
				<div class="right">完成可获取积分 ：10</div>
				<button class="fil-btn butns" id="facecard_btn" onclick=''>去完成</button>
			</div>
			<div class="border_line"> &nbsp;</div> 
		</div>
		<!-- 自助借书 -->
		<div class="patre">
			<div class="row">
				<div class="topfont col-sm-3">
					<div class="title">6.自助借书</div>
					<div class="fontqw">读者通过馆内自助借还书设备，至少完成一次借书任务。</div>
				</div>
				<img class="imga col-sm-3" src="plaf/images/img_6.png">
			 </div>
			 <hr class="lines">
			 <div class="row lints">
				<div class="right">完成可获取积分 ：10</div>
				<button class="fil-btn butns" id="loanbook_btn" onclick='cLoanBook()'>去完成</button>
			</div>
			<div class="border_line"> &nbsp;</div> 
		</div>
		<!-- 自助还书 -->
		<div class="patre">
			<div class="row">
				<div class="topfont col-sm-3">
					<div class="title">7.自助还书</div>
					<div class="fontqw">读者通过馆内自助借还书设备，至少完成一次还书任务。</div>
				</div>
				<img class="imga col-sm-3" src="plaf/images/img_7.png">
			 </div>
			 <hr class="lines">
			 <div class="row lints">
				<div class="right">完成可获取积分 ：10</div>
				<button class="fil-btn butns" id="returnbook_btn" onclick='cReturnBook()'>去完成</button>
			</div>
			<div class="border_line"> &nbsp;</div> 
		</div>
		<!-- 下载文章 -->
		<div class="patre">
			<div class="row">
				<div class="topfont col-sm-3">
					<div class="title">8.下载文章</div>
					<div class="fontqw">读者利用图书馆电子资源平台，在校外成功下载文章一次。</div>
				</div>
				<img class="imga col-sm-3" src="plaf/images/img_10.png">
			 </div>
			 <hr class="lines">
			 <div class="row lints">
				<div class="right">完成可获取积分 ：10</div>
				<button class="fil-btn butns" id="downloadqk_btn" onclick='cDownloadqk()'>去完成</button>
			</div>
			<div class="border_line"> &nbsp;</div> 
		</div>
		<!-- 参加活动 -->
		<div class="patre">
			<div class="row">
				<div class="topfont col-sm-3">
					<div class="title">9.参加活动</div>
					<div class="fontqw">参加活动后可获取相应积分</div>
				</div>
				<img class="imga col-sm-3" src="plaf/images/img_8.png">
			 </div>
			 <hr class="lines">
			 <div class="row lints">
				<div class="right">完成可获取积分 ：10</div>
				<button class="fil-btn butns" id="activity_btn" onclick="toActivity('${user.userId}')">去完成</button>
			</div>
			<div class="border_line"> &nbsp;</div> 
		</div>
		<!-- 调查问卷 -->
		<div class="patre" style="margin-bottom: 20%;">
			<div class="row">
				<div class="topfont col-sm-3">
					<div class="title">10.完成调查问卷</div>
					<div class="fontqw">完成调查问卷可以获取相应积分</div>
				</div>
				<img class="imga col-sm-3" src="plaf/images/img_9.png">
			 </div>
			 <hr class="lines">
			 <div class="row lints">
				<div class="right">完成可获取积分 ：50</div>
				<button class="fil-btn butns" id="questionnaire_btn" onclick="toQuestionnaire('${user.userId}')">去完成</button>
			</div>
			<div class="border_line"> &nbsp;</div> 
		</div>
	</div>
	
	<div class="foot_set row">
		<!-- left -->
		<button onclick="totrans_l()" class="left_s" style="pointer-events: none; cursor: default; opacity: 0.6;">
			<img src="plaf/images/shouye.png">
		</button>
		<!-- m -->
		<button onclick="totrans_m('${user.account}')" class="left_s">
			<img class="left_s" src="plaf/images/huodong.png"> 
		</button>
		<!-- right -->
		<button onclick="totrans_r()" class="left_s">
			<img class="left_s" src="plaf/images/wode.png">
		</button>
	</div>
</body>
</html>