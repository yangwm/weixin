<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<html lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>书刊检索</title>
	<meta name="keywords" content="书刊检索" />
	<meta name="description" content="广外图书馆书刊检索" />
	
	<link rel="shortcut icon" href="plaf/images/favicon.ico">
	<link rel="stylesheet" type="text/css" href="plaf/css/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="plaf/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="plaf/fonts/iconic/css/material-design-iconic-font.min.css">
	<link rel="stylesheet" type="text/css" href="plaf/css/util.css">
	<link rel="stylesheet" type="text/css" href="plaf/css/main.css">
	<link rel="stylesheet" href="plaf/js/jquery-ui/jquery-ui.min.css">
	<script src="plaf/js/jquery-3.6.0.min.js"></script>
    <script src="plaf/js/jquery-ui/jquery-ui.min.js"></script>
	<script src="plaf/js/main.js"></script>
</head>
<body>
	<div class="limiter">
		<div class="container-login100" style="background-image: url('plaf/images/bg-01.jpg');">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-25 p-b-24" id="login_div">
				<!-- <form class="login100-form validate-form"> -->
					<span class="login100-form-title p-b-25"><img alt="登录" src="plaf/images/coverphoto.png"></span>
					
					<span class="login-error-tips p-b-0" id="error_tips"></span>
					
					<div class="wrap-input100 validate-input m-b-23" data-validate="请输入账号">
						<!-- <span class="label-input100">用户名</span> -->
						<input class="input100" type="text" name="account" id="account" placeholder="请输入学号" autocomplete="off">
						<span class="focus-input100" data-symbol="&#xf206;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="请输入密码">
						<!-- <span class="label-input100">密码</span> -->
						<input class="input100" type="password" name="pass" id="pass" autocomplete="new-password" placeholder="请输入密码">
						<span class="focus-input100" data-symbol="&#xf190;"></span>
					</div>
					<div class="r-forget cl p-t-8 p-b-31">
						<span class="z_tips"><a href="javascript:void();" onclick="showUserAgree();" class="y">隐私权政策</a></span>
		                <!-- <a href="toRegister" class="z">账号注册 </a> -->
		                <a href="http://librra.gdufs.edu.cn/ermsClient/eresourceInfo.do?rid=1333" class="y">忘记密码?</a>
		            </div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn" id="loginbtn" onclick='fLogin();'>登 录</button>
						</div>
					</div>

		<!-- 			<div class="txt1 text-center p-t-25 p-b-5">
						<span>第三方登录</span>
					</div>

					<div class="flex-c-m">
						<a href="#" class="login100-social-item bg1"><i class="fa fa-wechat"></i></a>
						<a href="#" class="login100-social-item bg2"><i class="fa fa-qq"></i></a>
						<a href="#" class="login100-social-item bg3"><i class="fa fa-weibo"></i></a>
					</div> -->
					
				<!-- </form> -->
			</div>
		</div>
	</div>
	<div id="user_agree" title="请阅读隐私权政策"></div>

</body>
<script type="text/javascript">
var dialogUserAgree;//dialog
$(function(){
	$("#user_agree").load("plaf/userAgree.html",function(responseTxt,statusTxt,xhr){
		if(statusTxt=="success"){
	        //alert("外部内容加载成功！");
	    }
	    if(statusTxt=="error"){
	    	//alert("Error: "+xhr.status+": "+xhr.statusText);
	    }
	});
	dialogUserAgree=$("#user_agree").dialog({
		autoOpen:false,
	    //closeOnEscape:false,
	  	open:function(event,ui){$(".ui-dialog-titlebar-close").hide();},
	    resizable: false,
	    modal:true,
	    height:$(document.body).height(),
	    width:$(document.body).width(),
	    /*buttons: {
		         '确定': function() {
		       $( this ).dialog( "close" );
		   },
		          '取消': function() {
		       $( this ).dialog( "close" );
	        }
	    }*/
	});
});

function setTimer4agree() {
	var count = 5;
	$("#agree_id").text("已阅读(" + count + ")");
	$('#agree_id').attr('href', 'javascript:return false;');
	$('#agree_id').attr('style', 'font-weight:bold;font-size:18px;opacity:0.5;');
	var countdown = setInterval(timeDown, 1000);
	function timeDown() {
		$("#agree_id").html("已阅读(" + count + ")");
		if (count <= 0) {
			$("#agree_id").text("已阅读");
			$("#agree_id").attr('href', 'javascript:closeUserDiv();');
			$('#agree_id').attr('style', 'font-weight:bold;font-size:18px;');
			clearInterval(countdown);
			$("#agree_id").blur();
		}
		count--;
	}
}
function showUserAgree(){
	dialogUserAgree.dialog("open");
	setTimer4agree();
}

function closeUserDiv(){
	dialogUserAgree.dialog("close");
	var account = $('#account').val();
	if(account==""){
		return;
	}
	$.ajax({
		type: 'POST',
		async: true,
		data: 'sno=' + account,
		dataType: 'json',
		url: 'userAgree.htm',
		success: function(dmsg) {
			window.location.href = 'main.htm';
		}
	});
}

</script>
</html>