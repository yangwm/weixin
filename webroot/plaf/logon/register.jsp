<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<html lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>紫云读书社</title>
	<meta name="keywords" content="紫云读书社" />
	<meta name="description" content="广外图书馆 紫云读书社" />
	
	<link rel="shortcut icon" href="plaf/images/favicon.ico">
	<link rel="stylesheet" type="text/css" href="plaf/css/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="plaf/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="plaf/fonts/iconic/css/material-design-iconic-font.min.css">
	<link rel="stylesheet" type="text/css" href="plaf/css/util.css">
	<link rel="stylesheet" type="text/css" href="plaf/css/main.css">
</head>
<body>
	<div class="limiter">
		<div class="container-login100" style="background-image: url('plaf/images/bg-01.jpg');">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-25 p-b-24" id="register_div">
				<!-- <form class="login100-form validate-form"> -->
					<span class="login100-form-title p-b-25"><img alt="注册" src="plaf/images/zydss.png"></span>
					
					<span class="login-error-tips p-b-0" id="error_tips"></span>
					
					<div class="wrap-input100 validate-input m-b-23" data-validate="请输入手机">
						<!-- <span class="label-input100">用户名</span> -->
						<input class="input100" type="text" name="phone" id="phone" placeholder="请输入手机号" autocomplete="off">
						<span class="focus-input100" data-symbol="&#xf2cc;"></span>
					</div>
					
					<div class="wrap-input100 validate-input m-b-23" data-validate="请输入学号">
						<!-- <span class="label-input100">用户名</span> -->
						<input class="input100" type="text" name="account" id="account" placeholder="请输入学号" autocomplete="off">
						<span class="focus-input100" data-symbol="&#xf206;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="请输入密码">
						<!-- <span class="label-input100">密码</span> -->
						<input class="input100" type="password" name="pass" id="pass" autocomplete="new-password" placeholder="请输入密码">
						<span class="focus-input100" data-symbol="&#xf190;"></span>
					</div>
					<div class="r-forget cl p-t-8 p-b-31">
		                <!-- <a href="toRegister" class="z">账号注册 </a> -->
		                <a href="toLogin" class="y">已有帐号? 马上登录</a>
		            </div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn" id="registerbtn" onclick='fRegister();'>注 册</button>
						</div>
					</div>

					<!-- <div class="txt1 text-center p-t-25 p-b-5">
						<span>第三方登录</span>
					</div>

					<div class="flex-c-m">
						<a href="#" class="login100-social-item bg1"><i class="fa fa-wechat"></i></a>
						<a href="#" class="login100-social-item bg2"><i class="fa fa-qq"></i></a>
						<a href="#" class="login100-social-item bg3"><i class="fa fa-weibo"></i></a>
					</div> -->
					
				<!-- </form> -->
			</div>
		</div>
	</div>

	<script src="plaf/js/jquery-3.6.0.min.js"></script>
	<script src="plaf/js/main.js"></script>
</body>

</html>