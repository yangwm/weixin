/**
 *  前端统一的语言中文
 *  @author ys
 *  @create 2012-10-10
 *  @update 2012-11-30
 *  @qq 517757409
 *  @ver 1.3
 */

var vmissing_message = "该输入项必填";
var vint_message = "输入内容必须是整数，大小必须介于 <span style='color:red'>{0}</span> 和 <span style='color:red'>{1}</span> 之间";
var vfloat_message = "输入内容必须含有小数点，大小必须介于 <span style='color:red'>{0}</span> 和 <span style='color:red'>{1}</span> 之间";
var vfloatp_message = "精确至小数点后<span style='color:red'>{0}</span>位，大小必须介于 <span style='color:red'>{1}</span> 和 <span style='color:red'>{2}</span> 之间";
var vpositive_message = "输入内容必须是正数";
var vnegative_message = "输入内容必须是负数";
var email_message = "请输入有效的电子邮件账号(例：abc@126.com)";
var telephone_message = "请按照 区号-电话号码 的格式输入";
var mobile_message = "请输入正确的手机号码";
var zip_message = "请输入正确的邮政编码";
var idcard_message = "请用英文输入法输入正确的身份证或港澳回乡证或护照号码";
var chs_message = "输入内容必须是全是中文";
var egl_message = "输入内容必须是全是英文字母";
var not_equal_message = "<span style='color:red'>{2}</span> 不能等于 <span style='color:red'>{1}</span>";
var greater_than_message = "<span style='color:red'>{2}</span> 必须大于 <span style='color:red'>{1}</span>";
var less_than_message = "<span style='color:red'>{2}</span> 必须小于 <span style='color:red'>{1}</span>";
var greater_than_equal_message = "<span style='color:red'>{2}</span> 必须大于或者等于 <span style='color:red'>{1}</span>";
var less_than_equal_message = "<span style='color:red'>{2}</span> 必须小于或者等于 <span style='color:red'>{1}</span>";
var vsle_message = "输入的长度必须介于 <span style='color:red'>{0}</span> 和 <span style='color:red'>{1}</span> 之间";
var vpwd_message = "<span style='color:red'>{2}</span> 必须等于 <span style='color:red'>{1}</span>";
var nchs_message = "不能含有中文";
var username_message = "只能填写中文、英文字符和数字";
var numb_message = "所输数字必须介于 <span style='color:red'>{0}</span> 和 <span style='color:red'>{1}</span> 之间";
var vmobile_telephone = "所属号码在格式必须为手机或者　区号-电话号码　形式";
var password_message = "只能由英文字符或数字或下划线或横线组成";
var account_message = "只能由零个或多个英文字符开头5至11个数字结尾组成";