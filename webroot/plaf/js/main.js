//是否是邮箱
function isEmail(str) {
	var pos = new RegExp(/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/);
	return pos.test(str);
}
//是否是手机
function isMobile(str) {
	var pos = new RegExp(/^1[3456789]\d{9}$/);
	return pos.test(str);
}
function isPassword(str) {
	var pos = new RegExp(/^[A-Za-z0-9_-]+$/);
	return pos.test(str);
}
//英文字符开头和数字结尾
function isAccount(str) {
	//var pos = new RegExp(/^[A-Z]{0,3}\d{5,11}[A-Z]*\d{0,9}$/);
	var pos = new RegExp(/^[A-Za-z]*\d{0,11}[A-Za-z]*\d{0,9}$/);
	return pos.test(str);
}

/** 表单序列化成json的方法  */
function div2Json(divId) {
	var inputArray = $("#" + divId).find('input');
	var selectArray = $("#" + divId).find('select');
	/*请求参数转json对象*/
	var jsonObj = {};
	$(inputArray).each(function() {
		jsonObj[this.name] = this.value;
	});
	$(selectArray).each(function() {
		jsonObj[this.name] = this.value;
	});
	return jsonObj;
}

/** 表单序列化成json的方法  */
function form2Json(formId) {
	var paramArray = $('#' + formId).serializeArray();
	/*请求参数转json对象*/
	var jsonObj = {};
	$(paramArray).each(function() {
		jsonObj[this.name] = this.value;
	});
	//console.log(jsonObj);
	return jsonObj;
}
/** 表单序列化成json字符串的方法  */
function form2JsonString(formId) {
	//var ser=$('#'+formId).serialize();
	var paramArray = $('#' + formId).serializeArray();
	//console.log("----:"+paramArray);
	/*请求参数转json对象*/
	var jsonObj = {};
	$(paramArray).each(function() {
		jsonObj[this.name] = this.value;
	});
	//console.log(jsonObj);
	//console.log("+:"+JSON.stringify(jsonObj));
	// json对象再转换成json字符串
	return JSON.stringify(jsonObj);
}

function disabledAll() {
	$('input').attr("disabled", "disabled");//将input元素设置为disabled/readonly
	$('textarea').attr("disabled", "disabled");//将input元素设置为disabled/readonly
	$('select').attr("disabled", "disabled");//将select元素设置为disabled
}

function fLogin() {
	var account = $('#account').val();
	var pass = $('#pass').val();

	if (account == '') {
		$("#error_tips").html("账号不能为空！");
		return;
	}
	if (pass == '') {
		$("#error_tips").html("密码不能为空！");
		return;
	}

	var datas = div2Json('login_div');
	$('#loginbtn').attr("disabled", "disabled");
	$.ajax({
		type: 'POST',
		async: true,
		data: datas,
		dataType: 'json',
		url: 'login',
		success: function(dmsg) {
			$('#loginbtn').removeAttr("disabled");
			//var obj=eval("("+dmsg+")");
			if (dmsg.rtn == 0) {
				if(dmsg.license == 1){
					//alert('保存成功！');
					window.location.href = 'main.htm';
					//parent.showKeyword();
				}else{
					showUserAgree();
				}
			} else if (dmsg.rtn == -1) {
				$("#error_tips").html("账号和密码不能为空！");
			} else if (dmsg.rtn == -2) {
				//alert('参数异常！');
				$("#error_tips").html("账号或密码错误！");
			} else if (dmsg.rtn == -3) {
				//alert('参数异常！');
				$("#error_tips").html("疑似高频访问，请稍后再登录！");
			} else {
				$("#error_tips").html(dmsg.msg);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$('#loginbtn').removeAttr("disabled");
			$("#error_tips").html("操作失败，请联系管理员！");
			//alert("st1:"+XMLHttpRequest.status);
			//alert("st2:"+XMLHttpRequest.readyState);
			//alert("st3:"+textStatus);
			//alert("st4:"+errorThrown);
		}
	});
}

function fRegister() {
	var phone = $("#phone").val();
	var account = $("#account").val();
	var pass = $("#pass").val();

	if (phone == '') {
		$("#error_tips").html("手机号不能为空！");
		return;
	} else if (!isMobile(phone)) {
		$("#error_tips").html("手机号格式有误！");
		return;
	}

	if (account == '') {
		$("#error_tips").html("学号不能为空！");
		return;
	} else if (!isAccount(account)) {
		$("#error_tips").html("学号格式有误！");
		return;
	}

	if (pass == '') {
		$("#error_tips").html("密码不能为空！");
		return;
	} else if (!isPassword(pass)) {
		$("#error_tips").html("密码只能包含英文字符和数字！");
		return;
	}

	var datas = div2Json('register_div');
	$('#registerbtn').attr("disabled", "disabled");
	$.ajax({
		type: 'POST',
		async: true,
		data: datas,
		dataType: 'json',
		url: 'register',
		success: function(dmsg) {
			$('#registerbtn').removeAttr("disabled");
			//var obj=eval("("+dmsg+")");
			//console.log(dmsg);
			if (dmsg.rtn == 0) {
				//alert('保存成功！');
				window.location.href = 'toLogin';
				//parent.showKeyword();
			} else if (dmsg.rtn == -1) {
				$("#error_tips").html("手机号、学号和密码不能为空！");
			} else if (dmsg.rtn == -2) {
				$("#error_tips").html("手机号" + dmsg.phone + "已被注册！");
			} else if (dmsg.rtn == -3) {
				$("#error_tips").html("学号" + dmsg.account + "已被注册！");
			} else {
				$("#error_tips").html("注册失败，请联系管理员！");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$('#registerbtn').removeAttr("disabled");
			$("#error_tips").html("操作失败，请联系管理员！");
			//alert("st1:"+XMLHttpRequest.status);
			//alert("st2:"+XMLHttpRequest.readyState);
			//alert("st3:"+textStatus);
			//alert("st4:"+errorThrown);
		}
	});
}

function fCheck() {
	var phone = $("#phone").val();
	if (phone == '') {
		$("#error_tips").html("手机号不能为空！");
		return;
	} else if (!isMobile(phone)) {
		$("#error_tips").html("手机号格式有误！");
		return;
	}

	var datas = div2Json('reset_div');
	$('#codebtn').attr("disabled", "disabled");
	$.ajax({
		type: 'POST',
		async: true,
		data: datas,
		dataType: 'json',
		url: 'sendSMS',
		success: function(dmsg) {
			//$('#codebtn').removeAttr("disabled");
			//var obj=eval("("+dmsg+")");
			if (dmsg.rtn == 0) {
				setTimer();
			} else {
				$("#error_tips").html("验证码发送失败，请稍后再试！");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$('#codebtn').removeAttr("disabled");
			$("#error_tips").html("操作失败，请联系管理员！");
			//alert("st1:"+XMLHttpRequest.status);
			//alert("st2:"+XMLHttpRequest.readyState);
			//alert("st3:"+textStatus);
			//alert("st4:"+errorThrown);
		}
	});
}

function setTimer() {
	$('#codebtn').attr('disabled', 'disabled');
	var count = 3;
	$("#codebtn").html("获取验证码(" + count + ")");
	var countdown = setInterval(timeDown, 1000);
	function timeDown() {
		$("#codebtn").html("获取验证码(" + count + ")");
		if (count <= 0) {
			$("#codebtn").html("获取验证码");
			$("#codebtn").removeAttr("disabled");
			clearInterval(countdown);
			$("#codebtn").blur();
		}
		count--;
	}
}

function fReset() {
	var phone = $("#phone").val();
	var pass = $("#pass").val();
	var code = $("#code").val();

	if (phone == '') {
		$("#error_tips").html("手机号不能为空！");
		return;
	} else if (!isMobile(phone)) {
		$("#error_tips").html("手机号格式有误！");
		return;
	}

	if (pass == '') {
		$("#error_tips").html("密码不能为空！");
		return;
	} else if (!isPassword(pass)) {
		$("#error_tips").html("密码只能包含英文字符和数字！");
		return;
	}

	if (code == '') {
		$("#error_tips").html("验证码不能为空！");
		return;
	}

	var datas = div2Json('reset_div');
	$('#resetbtn').attr("disabled", "disabled");
	$.ajax({
		type: 'POST',
		async: true,
		data: datas,
		dataType: 'json',
		url: 'resetPwd',
		success: function(dmsg) {
			$('#resetbtn').removeAttr("disabled");
			//var obj=eval("("+dmsg+")");
			if (dmsg.rtn == 0) {
				//alert('保存成功！');
				window.location.href = 'toLogin';
			} else if (dmsg.rtn == -1) {
				$("#error_tips").html("手机号、密码和验证码不能为空！");
			} else if (dmsg.rtn == -2) {
				$("#error_tips").html("手机号" + dmsg.phone + "不存在！");
			} else if (dmsg.rtn == -3) {
				$("#error_tips").html("验证码" + dmsg.code + "校验失败！");
			} else {
				$("#error_tips").html("重置密码失败，请联系管理员！");
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$('#resetbtn').removeAttr("disabled");
			$("#error_tips").html("操作失败，请联系管理员！");
			//alert("st1:"+XMLHttpRequest.status);
			//alert("st2:"+XMLHttpRequest.readyState);
			//alert("st3:"+textStatus);
			//alert("st4:"+errorThrown);
		}
	});
}

//每日签到
function daily() {
	$.ajax({
		type: 'POST',
		async: true,
		data: 'uid=' + $('#userId').val(),//去页面对应 userId值
		dataType: 'json',
		url: 'dailyAttendance.htm',
		success: function(dmsg) {
			$('#resetbtn').removeAttr("disabled");
			//var obj=eval("("+dmsg+")");
			if (dmsg.rtn == 0) {
				$("#score_label").text(parseInt($("#score_label").text())+10);
				$("#daily_a").html('已签到');
				$('#daily_a').attr("onclick", "return false;");
				$('#daily_a').attr("class", "fil-btn butns location");
				//window.location.href = 'main.htm';
			} else {
				alert("签到失败，请联系管理员！");
			}
		}
	});
}
//打卡-读者登记
function cJoinRegister(account) {
	window.location.href = 'http://library.gdufs.edu.cn:1581/uums/usermanage!toUserInfo4Remote?sno='+account+'&tp=USERINFO&enc=4471221b20d62d5460f36b90b98506fc&dk=http://library.gdufs.edu.cn:1010/rc/main.htm';
	//window.location.href='http://localhost:8080/uums/usermanage!toUserInfo4Remote?sno=201710076&tp=USERINFO&enc=4471221b20d62d5460f36b90b98506fc&dk=http://localhost:9090/rc/main.htm';
}
//进入入馆常识学习页面
function cGeneralLearn() {
	window.location.href = 'generalLearn.htm';
}

//进入常识答题页面
function toQuestion() {
	window.location.href = 'toQuestion.htm';
}

//进入关注微信公众号页面
function toSubscribe(userId,flag) {
	if(flag==0){
		window.location.href = 'toSubscribe.htm';
	}else{
		$('#subscribe_btn').attr("disabled","disabled");
		$.ajax({
			type: 'POST',
			async: true,
			data: 'uid=' + userId,//去页面对应 userId值
			dataType: 'json',
			url: 'subscribe.htm',
			success: function(dmsg) {
				$('#subscribe_btn').removeAttr("disabled");
				//var obj=eval("("+dmsg+")");
				if (dmsg.rtn == 0) {
					window.location.href = 'toSubscribe.htm';
					//$("#subscribe_btn").html('已关注');
					//$("#subscribe_btn").attr('class', 'fil-btn-finish');
				} else {
					alert("签到失败，请联系管理员！");
				}
			}
		});
	}
}
function toActivity(sno) {
	var timestamp=new Date().getTime();
	var sign=hex_md5(sno+timestamp);
	var url="https://hd.chaoxing.com/?fid=21415&sno="+sno+"&ts="+timestamp+"&_sign="+sign;
	window.location.href = url;
}
function toQuestionnaire(sno) {
	//alert("功能暂未开放");
	window.location.href = 'toQuestionnaire.htm';
}
function totrans_l(){
	window.location.href = 'main.htm';
}
function totrans_m(sno){
	toActivity(sno);
}
function totrans_r(){
	window.location.href = 'mine.htm';
	
}

