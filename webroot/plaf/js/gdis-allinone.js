/**------------ cynthis file merge ------------**/
/**
 *  扩展方法
 *  @author ys
 *  @create 2012-10-16
 *  @update 2013-1-8
 *  @ver 1.5
 *  @qq 517757409
 */

var sid;
jQuery.extend({
	/**
	 * =================================================
	 * ================== 公共的方法  ====================
	 * ============= date：2012-10-17 =====================
	 * ============= author：ys ========================
	 * =================================================
	 */

	/**
	 * 通过input的name或者id取值
	 * @param {Object} nameorid
	 */
	getInputValByNameOrId : function(nameorid) {
		if (nameorid.indexOf('#') == 0) {
			return $(nameorid).val();
		} else {
			return $('input[name="' + nameorid + '"]').val();
		}
	},
	
	/**
	 * 选择框的回写数据方法
	 */
	selectDialogSetValue:function(tableId, simpleFlag){
		if(simpleFlag){
			return $.getMultiSelectData(tableId);
		}else{
			return $.getSimpleSelectData(tableId);
		}
	},
	
	/**
	 * 树形选择框的回写数据方法
	 */
	selectTreeDialogSetValue:function(tableId, simpleFlag){
		if(simpleFlag){
			return $.getMultiSelectTree(tableId);
		}else{
			return $.getSimpleSelectTree(tableId);
		}
	},
	
	/**
	 * 通过input的name获得选中的值
	 * @param {Object} name
	 */
	getInputCheckedByName : function(name) {
		return $('input[name="' + name + '"]:checked').val();
	},

	/**
	 * 通过input的name或者id取select的value
	 * @param {Object} nameorid
	 */
	getSvalueCheckedByNameOrId : function(nameorid) {
		if (nameorid.indexOf('#') == 0) {
			return $(nameorid + ' option:selected').val();
		} else {
			return $('select[name="' + nameorid + '"] option:selected').val();
		}
	},

	/**
	 * 通过input的name或者id取select的值
	 * @param {Object} nameorid
	 */
	getStextCheckedByNameOrId : function(nameorid) {
		if (nameorid.indexOf('#') == 0) {
			return $(nameorid + ' option:selected').text();
		} else {
			return $('select[name="' + nameorid + '"] option:selected').text();
		}
	},
	
	/**
	 * 是否选择了多条数据
	 */
	isSelectMultiData : function(tableid){
		var data = $("#" + tableid).datagrid("getSelections");
		 if(data.length>1){
			 return true;
		 }else{
			 return false;
		 }
	},
	

	/**
	 * 点击回车触发的事件
	 * @param {Object} callback
	 * @param {Object} target
	 */
	enterEventBinding : function(callback, target) {
		var tg;
		if (target) {
			tg = '#' + target;
		} else {
			tg = document;
		}
		$(tg).keypress(function(e) {
			if (e.which == 13 || e.which == 10) {
				callback();
			}
		});

	},
	
	/**
	 * 文件上传载入数据
	 */
	getFileAttachment : function(url,data,suce,isView){
		var ous = suce;
		if(typeof(suce)!='function'){
			ous = function(data, textStatus){
				$.each(data,function(index,da){
					if(isView){
						$("#" + suce).append("<tr ><td>" + da.attachmentName + "</td><td>"  + parseInt(da.fileSize/1024)  + "K</td><td><a href='attachment!getFileContentDownload?attachmentId=" + da.attachmentId + "' class='downloadTr'>下载</a><input type='hidden' name='attId' value='" + da.attachmentId + "'/></td></tr>");
					}else{
						$("#" + suce).append("<tr ><td>" + da.attachmentName + "</td><td>"  + parseInt(da.fileSize/1024)  + "K</td><td><a href='#' class='deleteTr'>删除</a>&nbsp;<a href='attachment!getFileContentDownload?attachmentId=" + da.attachmentId + "' class='downloadTr'>下载</a><input type='hidden' name='attId' value='" + da.attachmentId + "'/></td></tr>");
					}
				});
			}
		}
		$.ajax({
			url:url + '?now=' + Math.round(Math.random()*100),
			data:data,
			success:ous
		});
		
	},
	
	
	/**
	 * 页面的查询方法
	 */
	searchGrid : function(searchgrid, listgrid) {
		$("#" + listgrid).datagrid("load", $('#' + searchgrid).formJsonSer());
	},
	
	/**
	 * 回调数据赋值到什么地方，若有sid，则关闭窗口
	 */
	setCallbacks:function(key,value,sid){
		$(".window-body").find("iframe").contents().find('#' + key).val(value);
		if(sid){
			$('#' + sid).destroy();
		}
	},
	
	/**
	 *
	 * @param {Object} field
	 * @param {Object} title
	 * @param {Object} width
	 * @param {Object} formatter
	 */
	createColumns : function(field, title, width, formatter, align, rowspan, colspan) {
		this.field = field;
		this.title = title;
		this.width = width;
		this.formatter = formatter;

		if (align) {
			this.align = align;
		} else {
			this.align = 'center';
		}
		this.rowspan = rowspan;
		this.colspan = colspan;

		this.getField = function() {
			return this.field;
		}
		this.getTitle = function() {
			return this.title;
		}
		this.getWidth = function() {
			return this.width;
		}
		this.getFormatter = function() {
			return this.formatter;
		}
		this.getRowspan = function() {
			return this.rowspan;
		}
		this.getColspan = function() {
			return this.colspan;
		}
	},

	/**
	 *
	 */
	createToolbar : function(text, iconCls, handler) {
		this.text = text;
		this.iconCls = iconCls;
		this.handler = handler;

		this.getText = function() {
			return this.text;
		}
		this.getIconCls = function() {
			return this.iconCls;
		}
		this.getHandler = function() {
			return this.handler;
		}
	},
	/**
	 * =================================================
	 * ================== 弹出窗的方法  ===================
	 * ============= date：2012-10-20 ==================
	 * ============= author：ys ========================
	 * =================================================
	 */

	/**
	 * 回调赋值函数
	 * @param {Object} tableid
	 * @param {Object} pid
	 * @param {Object} cid
	 */
	setSimpleValFromCallbacks : function(tableid, pid, cid) {
		var objOptions = $("#" + tableid).datagrid("getSelected");
		var pids = pid.split('~');
		var cids;
		if (cid) {
			cids = cid.split('~');
		} else {
			cids = pids;
		}

		if (objOptions.length == 1) {
			for (var i = 0; i < pidslength; i++) {
				$('#' + pids[i]).val(eval('objOptions.' + cids[i]));
			};
		} else {
			$.info('请选择一条数据');
		}
	},

	changePage : function(vthis,val){
		var te = $('.pagination-num',$(vthis).parent().parent().parent().parent().parent());
		var e = jQuery.Event("keydown");//模拟一个键盘事件 
		e.keyCode = 13;//keyCode=13是回车 
		te.trigger(e);
	},
	
	
	/**
	 *
	 * @param {Object} tableid
	 * @param {Object} pid
	 * @param {Object} cid
	 */
	setMultiValFromCallbacks : function(tableid, pid, cid) {
		var objOptions = $("#" + tableid).datagrid("getSelections");
		var cids;
		if (cid) {
			cids = cid.split('~');
		} else {
			cids = pids;
		}

		if (objOptions) {
			for (var i = 0; i < pidslength; i++) {
				var cval;
				$.each(function(index, element) {
					cval += eval('objOptions.' + cid[i]) + ',';
				});
				$('#' + pid[i]).val(cval);
			}
		} else {
			$.info('请至少选择一条数据');
		}
	},
	
	/**
	 * 
	 */
	getChildByKey:function(key){
		return $(".window-body").find("iframe").contents().find('#' + key);
	},
	
	
	/**
	 * tableid
	 * @param {Object} tableid
	 */
	getSimpleSelectData : function(tableid) {
		return $("#" + tableid).datagrid("getSelected");
	},

	/**
	 * @param {Object} tableid
	 */
	getMultiSelectData : function(tableid) {
		return $("#" + tableid).datagrid("getSelections");
	},

	/**
	 *
	 */
	controlDataGridButton : function(tableid, fn) {
		$('#' + tableid).datagrid('options').onClickRow = fn;
		$('#' + tableid).datagrid('options').onCheckAll = fn;
		$('#' + tableid).datagrid('options').onUncheckAll = fn;
		$('#' + tableid).datagrid('options').onCheck = fn;
		$('#' + tableid).datagrid('options').onUncheck = fn;
		$('#' + tableid).datagrid('options').onLoadSuccess = fn;
	},

	/**
	 * 
	 * @param tableid
	 * @param colId
	 * @param url
	 * @param beforeParam
	 * @param df 回调方法
	 * @param dfParam1 回调方法的第一个参数
	 * @param dfParam2 回调方法的第二个参数
	 * @param dfParam3 回调方法的第三个参数
	 */
	delectGridData : function(tableid, colId, url, beforeParam, df, dfParam1, dfParam2, dfParam3) {
		var selects = $.getMultiSelectData(tableid);
		if (selects && selects.length > 0) {
			var Id = [];
			if (beforeParam && beforeParam != null) {
				Id = $.merge(Id, beforeParam);
			}
			for (var int = 0; int < selects.length; int++) {
				var element = selects[int];
				Id.push(eval('element.' + colId));
			}
			$.confirm('您确定删除选中的这些数据吗?', function(r) {
				if (r) {
					$.post(contextPath + '/' + url, {
						ids : Id.join(",")
					}, function(resp) {
						if (eval(resp.success)) {
							df(dfParam1, dfParam2, dfParam3);
						} else {
							var ers = '';
							$.each(resp.errors, function(index, data) {
								ers += data + '<br/>';
							});
							$.info("删除失败！<br/>" + ers);
						}
					});
				}
			});
		} else {
			$.info("您没有选择任何数据。");
		}
	},

	/**
	 * 创建一个窗体
	 * @param {Object} url
	 * @param {Object} title
	 * @param {Object} width
	 * @param {Object} height
	 * @param {Object} callbackFunc 弹出框成功执行业务逻辑，并关闭窗口时调用的方法名。字符串形式，请不注意不要带括号。
	 * @param {Object} callbackFuncParam1 回调方法的第一个参数。字符串形式。
	 * @param {Object} callbackFuncParam2 回调方法的第二个参数。字符串形式。
	 * @param {Object} callbackFuncParam3 回调方法的第三个参数。字符串形式。
	 */
	createWindow : function(url, title, width, height, callbackFunc, callbackFuncParam1, callbackFuncParam2, callbackFuncParam3) {
		var windid = $.win({
			url : url,
			title : title,
			width : width,
			height : height,
			closeBtn : true,
			buttons : [{
				id : 'button-id-save',
				text : '保存',
				iconCls : 'icon-save',
				handler : null
			},{
				id : 'button-id-ok',
				text : '确定',
				iconCls : 'icon-ok',
				handler : null
			}],
			onSuccess : callbackFunc,
			onSuccessParams : [callbackFuncParam1, callbackFuncParam2, callbackFuncParam3]
		});
		return windid;
	},
	
	/**
	 * 选择框和普通新增弹出框的区别在于，
	 * 1、它不通过刷新页面关闭窗口并返回，而是直接通过回调方式把值传回父窗口；
	 * 2、它的回调方法传的是方法对象，而不是字符串形式。
	 * 
	 * @param url
	 * @param title
	 * @param width
	 * @param height
	 * @param callbackFunc
	 * @param callbackFuncParam1
	 * @param callbackFuncParam2
	 * @param callbackFuncParam3
	 * @returns
	 */
	createSelectWindow : function(url, title, width, height, callbackFunc, callbackFuncParam1, callbackFuncParam2, callbackFuncParam3) {
		var windid = $.win({
			url : url,
			title : title,
			width : width,
			height : height,
			closeBtn : true,
			buttons : [{
				id : 'button-id-save',
				text : '保存',
				iconCls : 'icon-save',
				handler : null
			},{
				id : 'button-id-ok',
				text : '确定',
				iconCls : 'icon-ok',
				handler : null
			}],
			onSuccess : callbackFunc,
			onSuccessParams : [callbackFuncParam1, callbackFuncParam2, callbackFuncParam3],
			isSelectWindow : true
		});
		return windid;
	},
	
	/**
	 * 创建一个窗体。该窗体仅仅是用于展示信息。
	 * 和普通新增弹出框的区别在于：该窗口仅仅是用来展示信息，不会用来支持增删改查这些页面操作。
	 * 
	 * @param {Object} title
	 * @param {Object} contents
	 * @param {Object} width
	 * @param {Object} height
	 */
	createInfoWindow : function(title, contents, width, height) {
		var windid = $.win({
			title : title,
			contents : contents,
			width : width,
			height : height,
			closeBtn : true,
			isIframe : false
		});
		return windid;
	},

	/**
	 *
	 * @param {Object} text
	 * @param {Object} iconCls
	 * @param {Object} handlerfunction
	 */
	createButtons : function(text, iconCls, handler, id) {
		this.text = text;
		this.iconCls = iconCls;
		this.handler = handler;
		this.id = id;
		this.getText = function() {
			return text;
		};
		this.getIconCls = function() {
			return iconCls;
		};
		this.getHandler = function() {
			return handler;
		};
		this.getId = function() {
			return id;
		};
	},

	/**
	 *
	 * @param {Object} buttonsObjs
	 */
	changeToButtonsArray : function(buttonsObjs) {
		var buttonsArray = new Array();

		if (buttonsObjs) {
			if (buttonsObjs.length) {
				for (var i = 0; i < buttonsObjs.length; i++) {
					var ft = buttonsObjs[i].getHandler();
					buttonsArray.push({
						text : buttonsObjs[i].getText(),
						iconCls : buttonsObjs[i].getIconCls(),
						handler : ft
					});
				};
			}
		}
		return buttonsArray;
	},

	closeWindow : function(wid) {
		$('#' + wid).destroy();
	},

	/**
	 *
	 */
	reloadDataGrid : function(wid) {
		$("#" + wid).datagrid("reload");
		$(".panel[style*=block] .datagrid-header-check").children("input").attr("checked",false);
	},
	
	/**
	 * 
	 */
	reloadChildDataGrid : function(key){
		$(".window-body").find("iframe")[0].contentWindow.jQuery('#' + key).datagrid("reload");
	},
	/**
	 * =================================================
	 * ================== 树形列表的方法  =================
	 * ============= date：2012-10-25 ==================
	 * ============= author：ys ========================
	 * =================================================
	 */

	/**
	 *
	 */
	getSimpleSelectTree : function(tableid) {
		return $("#" + tableid).treegrid("getSelected");
	},

	/**
	 * 移动数据 
	 */
	changeMultiData : function(cd,nd){
		var cdd ;
		var ndd = $('#nd').datagrid('getData').rows;
		if('string'==typeof cd){
			cdd = $.getMultiSelectData(cd);	
			ndd = $('#nd').datagrid('getData').rows;
			for(var i = 0;i<cdd.length;i++){
				if($.inArray(cdd[i],ndd)==-1){
					$.merge(ndd,[cdd[i]]);
				};
			}
		}else{
			if($.inArray(cd,ndd)==-1){
				$.merge(ndd,[cd]);
			};
		}
		
		$('#' + nd).datagrid('loadData',ndd);
	},
	
	/**
	 *
	 */
	getMultiSelectTree : function(tableid) {
		return $("#" + tableid).treegrid("getSelections");
	},

	/**
	 * 刷新树形菜单
	 */
	reloadTreeGrid : function(tableid) {
		$("#" + tableid).treegrid("reload");
	},

	/**
	 *
	 */
	collapseTreeAll : function(tableid) {
		$('#' + tableid).treegrid('collapseAll');
	},

	/**
	 *
	 */
	expandTreeAll : function(tableid) {
		$('#' + tableid).treegrid('expandAll');
	},

	/**
	 *
	 */
	controlTreeGridButton : function(tableid, fn) {
		$('#' + tableid).treegrid('options').onClickRow = fn;
		$('#' + tableid).treegrid('options').onCheckAll = fn;
		$('#' + tableid).treegrid('options').onUncheckAll = fn;
		$('#' + tableid).treegrid('options').onCheck = fn;
		$('#' + tableid).treegrid('options').onUncheck = fn;
	},

	/**
	 *
	 */
	controlTreeGridButton : function(tableid, fn) {
		$('#' + tableid).treegrid('options').onClickRow = fn;
	},

	/**
	 *
	 */
	reloadTree : function(tableid) {
		var node = $('#' + tableid).treegrid('getSelected');

		if (node) {
			var param = 'node.' + $('#' + tableid).treegrid('options').idField;
			$('#' + tableid).treegrid('reload', eval(param));
		} else {
			return $('#' + tableid).treegrid('reload');
		}
	},

	/**
	 * treegrid的后台传参选定方法
	 */
	treegridCheck : function(treeid, children) {
		$.each(children, function(index, d) {
			if (d.children) {
				$.treegridCheck(treeid, d.children);
			}
			if (d.isChecked == 1) {
				$("#" + treeid).treegrid('select', d.PK);
			}
		});
	},

	/**
	 *
	 */
	getNodeChildren : function(tableid) {
		var node = $('#' + tableid).treegrid('getSelected');
		if (node) {
			var param = 'node.' + $('#' + tableid).treegrid('options').idField;
			return $('#' + tableid).treegrid('getChildren', eval(param));
		} else {
			return $('#' + tableid).treegrid('getChildren');
		}
	},

	/**
	 *
	 */
	getNodeParent : function(tableid) {
		var node = $('#' + tableid).treegrid('getSelected');
		if (node) {
			var param = 'node.' + $('#' + tableid).treegrid('options').idField;
			return $('#' + tableid).treegrid('getParent', eval(param));
		}
	},

	/**
	 *
	 */
	getNodeLevel : function(tableid) {
		var node = $('#' + tableid).treegrid('getSelected');
		if (node) {
			var param = 'node.' + $('#' + tableid).treegrid('options').idField;
			return $('#' + tableid).treegrid('getLevel', eval(param));
		}
	},

	/**
	 *
	 */
	delectTreeData : function(tableid, colId, url, df) {
		var selects = $.getSimpleSelectTree(tableid);
		if (selects) {
			$.confirm('您确定删除选中的数据吗?', function(r) {
				if (r) {
					$.post(contextPath + '/' + url, {
						ids : eval('selects.' + colId)
					}, function(resp) {
						if (eval(resp.success)) {
							df(resp);
						} else {
							var ers = '';
							$.each(resp.errors, function(index, data) {
								ers += data + '<br/>';
							});
							$.info("删除失败！<br/>" + ers);
						}
					});
				}
			});
		} else {
			$.info("您没有选择任何数据。");
		}

	},

	/**
	 * =================================================
	 * ================== 手风琴  ========================
	 * ============= date：2012-11-2 ===================
	 * ============= author：ys ========================
	 * =================================================
	 */
	createAccordion : function(title, content, selected, iconCls) {
		if (selected == true) {
			this.selected = true;
		} else {
			this.selected = false;
		}
		this.title = title;
		this.content = content;
		this.iconCls = iconCls;
	},

	/**
	 * =================================================
	 * ================== Tab页 ========================
	 * ============= date：2012-11-2 ===================
	 * ============= author：ys ========================
	 * =================================================
	 */
	createTabs : function(id, title, iconCls, href) {
		this.id = id;
		this.title = title;
		this.iconCls = iconCls;
		this.href = href;
		this.closable = true;
	},
	
	createClassicUITabs : function(id, title, iconCls, href) {
		this.id = id;
		this.title = title;
		this.iconCls = iconCls;
		this.closable = false;
		this.content = '<iframe id="tabIframe" name="tabIframe" scrolling="yes" frameborder="0"  src="' + href + '" style="width:100%;height:99.6%;" onLoad="fireIframeOnLoad();"></iframe>';
	},

	/**
	 *添加tabs
	 */
	addTabs : function(mainTabId, tbs) {
		$('#' + mainTabId).tabs('add', tbs);
	},

	/**
	 *
	 * @param {Object} id
	 * @param {Object} text
	 * @param {Object} lbfuntion
	 * @param {Object} iconCls
	 * @param {Object} plain
	 * @param {Object} disabled
	 */
	createLinkButton : function(id, text, lbfuntion, iconCls, plain, disabled) {
		$('#' + id).linkbutton({
			text : text,
			iconCls : iconCls,
			plain : plain,
			disabled : disabled
		});
		$('#' + id).bind('click', lbfuntion);
	},
	
	/**
	 * 
	 * @param msg
	 */
	info : function(msg) {
		top.$.messager.alert('提示', msg, 'info');
	},
	
	warn : function(msg) {
		top.$.messager.alert('警告', msg, 'warning');
	},
	
	error : function(msg) {
		top.$.messager.alert('错误', msg, 'error');
	},
	
	/**
	 * 
	 * @param msg
	 * @param callbackFunc
	 */
	confirm : function(msg, callbackFunc) {
		top.$.messager.confirm('请确认', msg, callbackFunc);
	}
});

(function($) {
	$.fn.extend({
		/**
		 *
		 * @param {Object} url
		 * @param {Object} width
		 * @param {Object} height
		 * @param {Object} idField
		 * @param {Object} treeField
		 * @param {Object} columns
		 */
		createTreegrid : function(url, width, height, toolbar, idField, treeField, columns, queryParams, frozenColumns, singleSelect) {

			var ss = true;
			if (singleSelect != undefined) {
				ss = false;
				var cs = [{
					checkbox : true
				}];
				if (frozenColumns) {
					frozenColumns = $.merge(cs, frozenColumns);
				} else {
					columns = $.merge(cs, columns);
				}
			}
			var th = this;
			
			return this.treegrid({
				checkbox : true,
				url : url,
				width : width,
				height : height,
				idField : idField,
				treeField : treeField,
				columns : [columns],
				queryParams:queryParams,
				frozenColumns : [frozenColumns],
				toolbar : toolbar,
				rownumbers : true,
				border : false,
				fit : true,
				singleSelect : ss,
				//pagination:true,
				autoRowHeight : false,
				fitColumns : true,
				animate : true,
				onCheck:function(data){
					if(!ss){
					if(data.children){
						$.each(data.children,function(index,data){
							th.treegrid('select',data.PK);
						});
					}
					}
				},
				onUncheck:function(data){
					if(!ss){
						if(data.children){
							$.each(data.children,function(index,data){
								th.treegrid('unselect',data.PK);
							});
						}
					}
				}
			});
		},

		/**
		 * 文件上传
		 */
		fileUpload : function(url,upFileType,updata, upsucess, fileSizeLimit,buttonText, width, hight, multi,onselect) {
			var uid = this.selector;
			var queueId = this.selector.substring(1) + '_queue';
			var fileSize = '20MB';
			if(fileSizeLimit){
				fileSize = fileSizeLimit;
			}
			var btext = '上传';
			if(buttonText){
				btext = buttonText;
			}
			var fileType = '*.*';
			if(upFileType =='image'){
				fileType = '*.jpg;*.gif;*.png';
			}
			if(upFileType =='txt'){
				fileType = '*.txt;*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.pptx';
			}
			if(upFileType =='userImg'){
				fileType = '*.jpg;*.gif;*.png';
			}
			
			var ous = upsucess;
			if(typeof(upsucess)!='function'){
				ous = function(file, data, response){
					if(updata.singleAttachement){
						$("#" + upsucess).empty();
					}
					if(updata.objId){
						$("#" + upsucess).append("<tr ><td>" + file.name + "</td><td>"  + parseInt(file.size/1024)  + "K</td><td><a href='#' class='deleteTr'>删除</a>&nbsp;<a href='attachment!getFileContentDownload?attachmentId=" + data + "' class='downloadTr'>下载</a><input type='hidden' name='attId' value='" + data + "'/></td></tr>");
					}else{
						$("#" + upsucess).append("<tr ><td>" + file.name + "</td><td>"  + parseInt(file.size/1024)  + "K</td><td><a href='#' class='deleteTr'>删除</a>&nbsp;<a href='attachment!getFileTempContentDownload?attTempId=" + data + "' class='downloadTr'>下载</a><input type='hidden' name='attTempId' value='" + data + "'/></td></tr>");
					}
				};
			}
			
			var os = onselect;
			if(typeof(onselect)!='function'){
				os = function centerUpload(file){
					if(upFileType =='userImg'){
						if($('#userHeadImg').attr('src').indexOf('defaultHeadPic')<0){
							if(confirm("上传图片将会覆盖原来的图片，您确定覆盖？")){
							 }else{
								 $(uid).uploadify('cancel');
							 }
						}
						}
				}
			}
			
			this.uploadify({
				'fileSizeLimit' : fileSize,
				'swf' : 'plat/js/uploadify/uploadify.swf',
				'uploader' : url,
				'formData' : updata,
				'removeTimeout' : 0,
				'buttonText' : btext,
				'width' : width,
				'height' : hight,
				'fileObjName' : 'uploadify',
				'queueID' :  queueId ,
				'multi' : multi,
				'queueSizeLimit':5,
				'fileTypeExts':fileType,
				'onSelect' : os,
				'onUploadSuccess' : ous
			});
			
			
		},
		/**
		 *
		 * @param {Object} data
		 */
		loadTree : function(data) {
			this.treegrid('loadData', data);
		},

		/**
		 * 创建列表的方法
		 * @param {Object} url
		 * @param {Object} width
		 * @param {Object} height
		 * @param {Object} toolbar
		 * @param {Object} frozenColumns
		 */
		createDategrid : function(url, width, height, toolbar, columns, queryParams, frozenColumns, singleSelect, fit) {
			var cs = [{
				checkbox : true
			}];
			if (frozenColumns) {
				frozenColumns = $.merge(cs, frozenColumns);
			} else {
				columns = $.merge(cs, columns);
			}

			var ft = true;
			if (fit != undefined) {
				ft = fit;
			}

			this.datagrid({
				border : false,
				url : url,
				width : width,
				height : height,
				toolbar : toolbar,
				fit : ft,
				striped : true,
				columns : [columns],
				queryParams:queryParams,
				frozenColumns : [frozenColumns],
				rownumbers : true,
				pagination : true,
				autoRowHeight : false,
				singleSelect : singleSelect,
				rownumbers : true,
				fitColumns : true,
				pageList : [15, 50, 200]
			});
		},
		
		
		/**
		 * 创建一棵树
		 * @param {Object} url
		 */
		createTree : function(url, checkbox) {
			this.tree({
				url : url,
				animate : true,
				lines : true,
				checkbox : checkbox
				// dnd:true,
				// cascadeCheck:true
				// onlyLeafCheck:true
			});
		},
		
		formJsonSer : function() {
			var arrayValue = $(this).serializeArray();
			var json = {};
			$.each(arrayValue, function() {
				var item = this;
				json[item["name"]] = item["value"];
			});
			return json;
		},
		/**
		 *
		 */
		addLoadSuccess : function(bt) {
			this.treegrid('options').onLoadSuccess = bt;
		}
	});
})(jQuery);
/**------------ cynthis file merge ------------**/
(function ($) {
    /**
     * 创建UUID
     */
    function S4() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }
    /**
     * 生成windowId
     */
    function createIndentityWindowId() {
        return "UUID-" + (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    }
    /**
     * 销毁
     */
    function destroy(target) {
        $(target).dialog("destroy");
    }
		
    /**
     * 获取当前操作的window
     *  @param target 当前窗口的windowId 或者 当前窗口中的元素(element)
     */
    function getWindow(target) {
        if (typeof target == "string") {
            return document.getElementById(target);
        } else {
            return $(target).closest(".window-body");
        }
    }
    
    //入口方法
    $.win = function (options) {
		/* 定义url */    	
        if (!options.url && !options.contents) {
            $.info("缺少必要参数!(url or content)");
            return false;
        }
        
        var windowId = createIndentityWindowId();
        if (options.id) {
            windowId = options.id;
        }else{
            options.id = windowId;
        }
			
        //如果存在buttons(请看easyui对buttons的定义)，默认添加关闭按钮
        if(options.closeBtn){
	        var defaultBtn = [{
	        	id : 'button-id-cancel',
	            text : '关闭',
	            iconCls : 'icon-cancel',
	            handler : function () {
	                $("#" + windowId).dialog("close");
	            }
	        }
	        ];
	        if(options.buttons){
	        	$.merge(options.buttons || [], defaultBtn);	
	        }else{
	        	options.buttons = defaultBtn;
	        }
	        
	        
        }
        options = $.extend({}, $.win.defaults, options || {});
        
        //修改按钮ID为唯一
        $.each(options.buttons, function(idx, button) {
        	options.buttons[idx].id = button.id + "_" + options.id;
        });
		
        if (options.isMax) {
            options.draggable = false;
        }
		//创建div
        var dialog = $('<div/>').attr("id", windowId);
        if(options.bodyCls) {
            dialog.addClass(options.bodyCls);
        }
        
        dialog.appendTo($(options.target));
		
        dialog.dialog($.extend({},options,{
            onClose : function () {
                if (typeof options.onClose == "function") {
                    options.onClose.call(dialog);
                }
                destroy(this);
            },
            onMove : function (left, top) {
                if (typeof options.onMove == "function") {
                    options.onMove.call(dialog);
                }
                var o = $.data(this, 'panel').options;
                if (top < 0) {
                    $(this).dialog("move", {
                        "left" : left,
                        "top" : 0
                    });
                } else if (o.maximized) {
                    $(this).dialog("restore");
                    $(this).dialog("move", {
                        "left" : left + 100,
                        "top" : top
                    });
                }
                if (top > ($(o.target).height() - 20)) {
                    $(this).dialog("move", {
                        "left" : left,
                        "top" : ($(o.target).height() - 25)
                    });
                }
            }
        }));
        if (options.align) {
            var w = dialog.closest(".window");
            switch (options.align) {
                case "right":
                    dialog.dialog("move", {
                        left : w.parent().width() - w.width() - 10
                    });
                    break;
                case "tright":
                    dialog.dialog("move", {
                        left : w.parent().width() - w.width() - 10,
                        top : 0
                    });
                    break;
                case "bright":
                    dialog.dialog("move", {
                        left : w.parent().width() - w.width() - 10,
                        top : w.parent().height() - w.height() - 10
                    });
                    break;
                case "left":
                    dialog.dialog("move", {
                        left : 0
                    });
                    break;
                case "tleft":
                    dialog.dialog("move", {
                        left : 0,
                        top : 0
                    });
                    break;
                case "bleft":
                    dialog.dialog("move", {
                        left : 0,
                        top : w.parent().height() - w.height() - 10
                    });
                    break;
                case "top":
                    dialog.dialog("move", {
                        top : 0
                    });
                    break;
                case "bottom":
                    dialog.dialog("move", {
                        top : w.parent().height() - w.height() - 10
                    });
                    break;
            }
        }
        
        dialog.find("div.dialog-button").css("text-align",options.btnPos);
        
        if (options.isMax) {
            dialog.dialog("maximize");
            dialog.dialog("open");
        }
        
        if($.fn.mask && options.mask) dialog.mask();
        
        //隐藏按钮
        $.each(options.buttons, function(idx, button) {
        	if (button.id != "button-id-cancel_" + options.id) {
        		$('#' + button.id).hide();
        	}
        });
        
        var atype = 'post';
        if(options.type=='GET'){
        	atype = 'get';
        }
        
        //关于内容
        if (options.contents) {
            ajaxSuccess(options.contents);
        } else {
            if (!options.isIframe) {
                $.ajax({
                    url : options.url,
                    type : atype,
                    data : options.data == null ? "" : options.data,
                    success : function (data) {
                        ajaxSuccess(data);
                    },
                    error : function () {
                        $.info("加载失败！");
                    }
                });
            } else {
                ajaxSuccess();
            }
        }
        return windowId;

        /**
         * 页面加载成功处理
         */
        function ajaxSuccess(data) {
            if (options.isIframe && !data) {
            	var vUrl = options.url;
            	if (vUrl.indexOf("?") == -1) {
            		vUrl += "?gdis-jq-window-id=" + options.id;
            	} else {
            		vUrl += "&gdis-jq-window-id=" + options.id;
            	}
            	
            	var vOnLoadParam = '\'' + options.id + '\''; 
            	if (options.isSelectWindow == undefined) {
	            	vOnLoadParam += ', \'' + options.onSuccess + '\'';
	            	$.each(options.onSuccessParams, function(idx, vParam) {
	            		if (vParam != undefined) {
	            			vOnLoadParam += ', \'' + vParam + '\'';
	            		}
	            	});
            	} else {
            		vOnLoadParam += ', \'\'';
	            	$.each(options.onSuccessParams, function(idx, vParam) {
	            		if (vParam != undefined) {
	            			vOnLoadParam += ', \'\'';
	            		}
	            	});
            	}
                dialog.find("div.dialog-content").html('<iframe width="100%" height="99%" frameborder="0" src="' + vUrl + 
                		'" onload="onDialogNestedIFrameOnload(' + vOnLoadParam + ');"></iframe>');
            } else {
                dialog.find("div.dialog-content").html(data);
            }
            $.parser.parse(dialog);
            options.onComplete.call(this, dialog, windowId, options.isSelectWindow, options.onSuccess, options.onSuccessParams);
            if($.fn.mask && options.mask) dialog.mask("hide");
        }
    };
    
    //关闭并销毁实体
    $.fn.destroy = function () {
        destroy(this);
    };
		
    window.GETWIN = getWindow;
		
    //默认参数
    $.win.defaults = $.extend({}, $.fn.dialog.defaults, {
        url : '', //窗口要加载的html片段地址
        data : '', //可带参数，data类型为jqurey.ajax的data参数类型
        target : 'body', //指定窗口打开的区域,是一个jq的选择器，例如#id
        height : 200,
        width : 400,
        collapsible : false,
        minimizable : false,
        maximizable : false,
        closable : true,
        modal : true,
        shadow : false,
        mask:true,
        resizable:false,
        closeBtn:false,
        inline: true,
        btnPos: 'right',
        isIframe:true,
        onComplete : function (dialog, windowId, isSelectWindow, onSuccess, onSuccessParams) {
        	//alert("isSelectWindow: " + isSelectWindow + ", (isSelectWindow == undefined) :" + (isSelectWindow == undefined))
        	$.each(dialog.find("div.dialog-button").find("a"), function(idx, button) {
        		if (button.id.indexOf("button-id-cancel") == -1) {
        			if (isSelectWindow == undefined) {
		        		$('#' + button.id).bind("click", function() {
		        			$('#' + windowId).find("iframe")[0].contentWindow.onSaveClick();
		        		});
        			} else {
        				$('#' + button.id).bind("click", function() {
		        			var vData = $('#' + windowId).find("iframe")[0].contentWindow.onSaveClick();
		        			onSuccess(vData, windowId, onSuccessParams[0], onSuccessParams[1], onSuccessParams[2]);
		        		});
        			}
        		}
        	});
        },
    	onClose : function () {},
    	onSuccess : '', //弹出框成功执行业务逻辑，并关闭窗口时调用的方法。
    	onSuccessParams : []
    });
})(jQuery);

/**
 * Iframe加载onLoad回调
 * 
 * @param wid 窗口ID
 * @param onSuccess 回调方法。字符串形式。
 * @param onSuccessParam1 回调方法的第一个参数。字符串形式。
 * @param onSuccessParam2 回调方法的第二个参数。字符串形式。
 * @param onSuccessParam3 回调方法的第三个参数。字符串形式。
 */
function onDialogNestedIFrameOnload(wid, onSuccess, onSuccessParam1, onSuccessParam2, onSuccessParam3) {
	var vForms = $('#' + wid).find("iframe").contents().find("form");
	if (vForms.length <= 0) {
		vForms = $('<form></form>').appendTo($('#' + wid).find("iframe").contents().find("body"));
	}
	var oWindowIdInput = $('<input type="hidden" id="gdis-jq-window-id" name="gdis-jq-window-id" />');
	oWindowIdInput.attr("value", wid);
	oWindowIdInput.appendTo(vForms);
	
	var vValue = "";
	var onSuccessParams = [onSuccessParam1, onSuccessParam2, onSuccessParam3];
	var counter = 0;
	$.each(onSuccessParams, function(idx, vParam) {
		if (vParam != undefined) {
			if (counter != 0) {
				vValue += ", ";
			}
			vValue += '"' + vParam + '"';
			counter++;
		}
	});
	if (vValue != "") {
		vValue = onSuccess + "(" + vValue + ")";
	} else {
		vValue = onSuccess;
		if (onSuccess.indexOf(")") == -1) {
			vValue += "()";
		}
	}
	
	var oSuccessInput = $('<input type="hidden" id="gdis-jq-window-onSuccess" name="gdis-jq-window-onSuccess" />');
	oSuccessInput.attr("value", vValue);
	oSuccessInput.appendTo(vForms);
}

/** 弹出框父窗口触发子窗口方法 **/
var saveMethod;
var saveParam;
function attachSaveEvent(meth, param1, param2, param3, param4, param5) {
	saveMethod = meth;
	saveParam = new Array(param1, param2, param3, param4, param5);
}

/**
 * 触发回调方法
 */
function onSaveClick() {
	try {
		if (saveMethod) {
			if (saveParam && saveParam.length == 5) {
				return saveMethod(saveParam[0], saveParam[1], saveParam[2], saveParam[3], saveParam[4]);
			} else {
				return saveMethod();
			}
		}
	} catch (e) {
	}
}

/**
 * 显示保存按钮。弹出框页面需要再document onload / jquery document ready事件中调用本地的showSaveButton方法。
 */
function showSaveButton(meth, param1, param2, param3, param4, param5) {
	attachSaveEvent(meth, param1, param2, param3, param4, param5);
	var vButtonId = '#button-id-save_' + vGlobalWindowId;
	//alert("vButtonId: " + vButtonId)
	parent.$(vButtonId).show();
}

/**
 * 显示确定按钮。弹出框页面需要再document onload / jquery document ready事件中调用本地的showOkButton方法。
 */
function showOkButton(meth, param1, param2, param3, param4, param5) {
	attachSaveEvent(meth, param1, param2, param3, param4, param5);
	var vButtonId = '#button-id-ok_' + vGlobalWindowId;
	parent.$(vButtonId).show();
}

/**
 * 隐藏所有业务按钮，例如保存按钮和确定按钮，但是关闭按钮不在此列。
 */
function hideAllBusinessButtons() {
	if (parent != undefined) {
		$.each(parent.$("div.dialog-button").find("a"), function(idx, button) {
			if (button.id.indexOf("button-id-cancel") == -1) {
				$(button).hide();
			}
		});
	}
}

/**------------ cynthis file merge ------------**/
// 是否为IE浏览器
var isMSIE = true;  //default MSIE
if (navigator.userAgent && navigator.userAgent.indexOf("MSIE") != -1) {
	isMSIE = true;
} else {
	isMSIE = false;
}

/**
 * 去掉左边空字符
 * 
 * @param str
 * @returns
 */
function lTrim(str) {
	var whitespace = new String(" \t\n\r");
	if (!isMSIE) {
		whitespace = new String(
				" \n\r\t\v\f\u00a0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000");
	}
	var s = new String(str);
	if (whitespace.indexOf(s.charAt(0)) != -1) {
		var j = 0, i = s.length;
		while (j < i && whitespace.indexOf(s.charAt(j)) != -1) {
			j++;
		}
		s = s.substring(j, i);
	}
	return s.toString();
}

/**
 * 去掉右边空字符
 * 
 * @param str
 * @returns
 */
function rTrim(str) {
	var whitespace = new String(" \t\n\r");
	if (!isMSIE) {
		whitespace = new String(
				" \n\r\t\v\f\u00a0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000");
	}
	var s = new String(str);
	if (whitespace.indexOf(s.charAt(s.length - 1)) != -1) {
		var i = s.length - 1;
		while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1) {
			i--;
		}
		s = s.substring(0, i + 1);
	}
	return s.toString();
}

/**
 * 去掉左右边空字符
 * 
 * @param str
 * @returns
 */
function trim(str) {
	return rTrim(lTrim(str)).toString();
}

/**
 * 是否为空字符串
 * 
 * @param s
 * @returns {Boolean}
 */
function isEmptyString(s) {
	if ((s == undefined) || (s == null) || (trim(s) == "")) {
		return true;
	} else {
		return false;
	}
}

/**
 * 定位到另外的页面。目前仅用于经典刷新页面的交互方式。
 */
function doGoto(vUrl) {
	if (isRichClientUIEnabled) {
		return;
	}
	
	var vMtdiv = top.$('#mtdiv');
	if (vMtdiv.tabs('tabs').length != 0) {
		var tab = vMtdiv.tabs('getSelected');
		var vOptions = tab.panel('options');
		var vTab = new top.$.createClassicUITabs(vOptions.id, vOptions.title, vOptions.iconCls, contextPath + vUrl);
		vMtdiv.tabs('update', {
			tab: tab,
			options: vTab
		});
	}
	//window.location.href = contextPath + vUrl;
}
/**避免重复提交表单，提交按钮在提交后变为disable**/
function buttonDisabled() {
	$('.saveButton').linkbutton('disable');
}
/**验证不通过时去掉disable**/
function buttonEnabled() {
	$('.saveButton').linkbutton('enable');
}
/**------------ cynthis file merge ------------**/
/**
 * 公用用户选择框（多选）
 * 
 * @param vUserId 
 * @param vTitle
 * @param vCallbackFunc
 */
function openCommonUserSelectDialog(vUserId, vTitle, vCallbackFunc) {
	var vUrl = contextPath + '/usermanage!multiUserSelect';
	if (vUserId != undefined && vUserId != null) {
		vUrl += "?userId=" + vUserId;
	}
	$.createSelectWindow(vUrl, vTitle, 500, 400, vCallbackFunc);
}

/**
 * 公用用户选择框（单选）
 * 
 * @param vUserId 
 * @param vTitle
 * @param vCallbackFunc
 */
function openSingleUserSelectDialog(vUserId, vTitle, vCallbackFunc, data1) {
	var vUrl = contextPath + '/usermanage!singleUserSelect';
	if (vUserId != undefined && vUserId != null) {
		vUrl += "?userId=" + vUserId;
	}
	$.createSelectWindow(vUrl, vTitle, 500, 400, vCallbackFunc, data1);
}
/**
 * 打开职位弹出框
 * 
 * @param vTitleId
 * @param vIsView 是否为查看操作。该参数只有在vTitleId不为空时有效。
 * @param vTitle
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 * @param vCallbackFunc 弹出框成功执行业务逻辑，并关闭窗口时调用的方法。字符串形式。
 * @param vCallbackFuncParam1 回调方法的第一个参数。字符串形式。
 * @param vCallbackFuncParam2 回调方法的第二个参数。字符串形式。
 * @param vCallbackFuncParam3 回调方法的第三个参数。字符串形式。
 */
function openTitleDialog(vTitleId, vIsView, vTitle, vMustOpenAsDialog, vCallbackFunc, vCallbackFuncParam1, vCallbackFuncParam2, vCallbackFuncParam3) {
	var vUrl = contextPath + '/sysTitle!showInfo';
	if (vTitleId != undefined && vTitleId != null) {
		vUrl += "?titleId=" + vTitleId;
		
		if (typeof(vIsView) == "boolean" && vIsView) {
			vUrl += "&mandatoryView=true"; 
		}
	}
	
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, vTitle, 520, 360, vCallbackFunc, vCallbackFuncParam1, vCallbackFuncParam2, vCallbackFuncParam3);
	} else {
		window.location.href = vUrl;
	}
}

/**
 * 打开菜单弹出框
 * 
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 */
function openMenuDialog(vFlag, vMenuId, vMenuKey, vTitle, vMustOpenAsDialog, vCallbackFunc) {
	var vUrl = contextPath + '/menu!showInfo';
	if(vFlag == "add"){
		if (vMenuId != undefined && vMenuId != null) {
			vUrl += "?parentMenuId=" + vMenuId;
			if (vMenuKey != undefined && vMenuKey != null) {
				vUrl += "&parentMenuKey=" + vMenuKey;
			}
		}
	}else if(vFlag == "edit"){
		if (vMenuId != undefined && vMenuId != null) {
			vUrl += "?menuId=" + vMenuId;
		}
	}else if(vFlag == "view"){
		if (vMenuId != undefined && vMenuId != null) {
			vUrl += "?mandatoryView=true&menuId=" + vMenuId;
		}
	}
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, vTitle, 520, 420, vCallbackFunc);
	} else {
		window.location.href = vUrl;
	}
}

/**
 * 打开工作组弹出框
 * 
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 */
function openGroupDialog(vGroupId, vIsView, vTitle, vMustOpenAsDialog, vCallbackFunc) {
	var vUrl = contextPath + '/sysGroup!showInfo';
	if (vGroupId != undefined && vGroupId != null) {
		vUrl += "?groupId=" + vGroupId;
		if (typeof(vIsView) == "boolean" && vIsView) {
			vUrl += "&mandatoryView=true";
		}
	}
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, vTitle,520, 360, vCallbackFunc);
	} else {
		window.location.href = vUrl;
	}
}

/**
 * 打开“工作组”-成员管理弹出框
 * 
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 */
function openGroupMemberDialog(vGroupId, vTitle, vMustOpenAsDialog, vCallbackFunc) {
	var vUrl = contextPath + '/sysGroup!toMemberManage';
	if (vGroupId != undefined && vGroupId != null) {
		vUrl += "?groupId=" + vGroupId;
	}
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, vTitle,600, 505, vCallbackFunc);
	} else {
		window.location.href = vUrl;
	}
}

/**
 * 打开“工作组管理”-选择主负责人弹出框
 */
function openGroupLeaderSelectDialog(vUserId, vTitle, vCallbackFunc) {
	var vUrl = contextPath + '/usermanage!multLeaderSelect';
	if (vUserId != undefined && vUserId != null) {
		vUrl += "?userId=" + vUserId;
	}
	$.createSelectWindow(vUrl, vTitle, 500, 400, vCallbackFunc);
}

/**
 * 打开“工作组管理”-管理成员-添加成员弹出框
 */
function openGroupMemberSelectDialog(vUserId, vTitle, vCallbackFunc) {
	openCommonUserSelectDialog(vUserId, vTitle, vCallbackFunc);
}

/**
 * 打开项目弹出框
 * 
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 */
function openProjectDialog(vProjectId, vIsView, vTitle, vMustOpenAsDialog, vCallbackFunc) {
	var vUrl = contextPath + '/sysProject!showInfo';
	if (vProjectId != undefined && vProjectId != null) {
		vUrl += "?projectId=" + vProjectId;
		if (typeof(vIsView) == "boolean" && vIsView) {
			vUrl += "&mandatoryView=true";
		}
	}
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, vTitle,520, 360, vCallbackFunc);
	} else {
		window.location.href = vUrl;
	}
}

/**
 * 打开积分弹出框
 * 
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 */
function openCreditsDialog(vSeqId, vIsView, vTitle, vMustOpenAsDialog, vCallbackFunc) {
	var vUrl = contextPath + '/sysCredits!showInfo';
	if (vSeqId != undefined && vSeqId != null) {
		vUrl += "?seqId=" + vSeqId;
		if (typeof(vIsView) == "boolean" && vIsView) {
			vUrl += "&mandatoryView=true";
		}
	}
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, vTitle,520, 360, vCallbackFunc);
	} else {
		window.location.href = vUrl;
	}
}

/**
 * 打开双轨制方案弹出框
 * 
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 */
function openMTrackDialog(vSeqId, vIsView, vTitle, vMustOpenAsDialog, vCallbackFunc) {
	var vUrl = contextPath + '/mTrack!showInfo';
	if (vSeqId != undefined && vSeqId != null) {
		vUrl += "?account=" + vSeqId;
		if (typeof(vIsView) == "boolean" && vIsView) {
			vUrl += "&mandatoryView=true";
		}
	}
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, vTitle,520, 360, vCallbackFunc);
	} else {
		window.location.href = vUrl;
	}
}

/**
 * 打开角色弹出框
 * 
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 */
function openRoleDialog(vRoleId, vIsView, vTitle, vMustOpenAsDialog, vCallbackFunc) {
	var vUrl = contextPath + '/rolemanage!showInfo';
	if (vRoleId != undefined && vRoleId != null) {
		vUrl += "?roleId=" + vRoleId;
		if (typeof(vIsView) == "boolean" && vIsView) {
			vUrl += "&mandatoryView=true";
		}
	}
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, vTitle, 520, 360, vCallbackFunc);
	} else {
		window.location.href = vUrl;
	}
}

/**
 * 打开角色成员弹出框
 * 
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 */
function openRoleMemberDialog(vRoleId, vTitle, vMustOpenAsDialog, vCallbackFunc) {
	var vUrl = contextPath + '/rolemanage!toMemberManage';
	if (vRoleId != undefined && vRoleId != null) {
		vUrl += "?roleId=" + vRoleId;
	}
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, vTitle, 600, 450, vCallbackFunc);
	} else {
		window.location.href = vUrl;
	}
}

/**
 * 打开“角色管理”-管理成员-添加用户成员弹出框
 */
function openRoleMemberUserSelectDialog(vUserId, vTitle, vCallbackFunc) {
	openCommonUserSelectDialog(vUserId, vTitle, vCallbackFunc);
}

/**
 * 打开“角色管理”-管理成员-添加工作组成员弹出框
 */
function openRoleMemberGroupSelectDialog(vGroupId, vTitle, vCallbackFunc) {
	var vUrl = contextPath + '/rolemanage!toMultGroupSelect';
	if (vGroupId != undefined && vGroupId != null) {
		vUrl += "?groupId=" + vGroupId;
	}
	$.createSelectWindow(vUrl, vTitle, 450, 400, vCallbackFunc);
}

/**
 * 打开“角色管理”-管理成员-添加部门成员弹出框
 */
function openRoleMemberCompanySelectDialog(vCompanyId, vTitle, vCallbackFunc, onSuccessParam1) {
	var vUrl = contextPath + '/rolemanage!toMultCompanySelect';
	if (vCompanyId != undefined && vCompanyId != null) {
		vUrl += "?companyId=" + vCompanyId;
	}
	$.createSelectWindow(vUrl, vTitle, 450, 400, vCallbackFunc, onSuccessParam1);
}

/**
 * 打开“角色管理”-管理成员-添加部门成员弹出框
 */
function openRoleMemberCompanySelectTree(vCompanyId, vTitle, vCallbackFunc) {
	var vUrl = contextPath + '/sysCompany!toSyscompanyTreeSelect?memberType=MT03';
	$.createSelectWindow(vUrl, vTitle, 450, 400, vCallbackFunc);
}

/**
 * 打开角色授权弹出框
 * 
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 */
function openRoleAuthorizeDialog(vRoleId, vTitle, vMustOpenAsDialog, vCallbackFunc) {
	var vUrl = contextPath + '/rolemanage!toPermissionManage';
	if (vRoleId != undefined && vRoleId != null) {
		vUrl += "?roleId=" + vRoleId;
	}
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, vTitle, 600, 450, vCallbackFunc);
	} else {
		window.location.href = vUrl;
	}
}

/**
 * 打开功能操作弹出框
 * 
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 */
function openFuncOperDialog(vFlag, vFuncId, vIsLeaf, vTitle, vMustOpenAsDialog, vCallbackFunc) {
	var vUrl = contextPath + '/func!showInfo';
	if(vFlag == "add"){
		if (vFuncId != undefined && vFuncId != null) {
			vUrl += "?funcId=" + vFuncId;
			if (vIsLeaf != undefined && vIsLeaf != null) {
				vUrl += "&isLeaf=" + vIsLeaf;
			}
		}
	}else if(vFlag == "edit"){
		if (vFuncId != undefined && vFuncId != null) {
			vUrl += "?funcOperId=" + vFuncId;
			if (vIsLeaf != undefined && vIsLeaf != null) {
				vUrl += "&isLeaf=" + vIsLeaf;
			}
		}
	}else if(vFlag == "view"){
		if (vFuncId != undefined && vFuncId != null) {
			vUrl += "?mandatoryView=true&funcOperId=" + vFuncId;
			if (vIsLeaf != undefined && vIsLeaf != null) {
				vUrl += "&isLeaf=" + vIsLeaf;
			}
		}
	}
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, vTitle, 600, 450, vCallbackFunc);
	} else {
		window.location.href = vUrl;
	}
}

/**
 * 打开用户弹出框
 * 
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 */
function openUserDialog(vUserId, vTitle, vMustOpenAsDialog, vCallbackFunc,vIsView,vParams) {
	var vUrl = contextPath + '/usermanage!showInfo';
	if (vUserId != undefined && vUserId != null) {
		vUrl += "?userId=" + vUserId;
	}
	if (typeof(vIsView) == "boolean" && vIsView) {
		vUrl += "&mandatoryView=true";
	}
	if (vParams != undefined && vParams != null) {
		vUrl += vParams;
	}
	
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		if(vUserId==1){
			$.createWindow(vUrl, vTitle, 400, 325, vCallbackFunc);
		}else{
			$.createWindow(vUrl, vTitle, 625, 580, vCallbackFunc);
		}
	} else {
		window.location.href = vUrl;
	}
}

function openUserDialog4Personal(vUserId, vTitle, vMustOpenAsDialog, vCallbackFunc,vIsView) {
	var vUrl = contextPath + '/usermanage!showInfo4Personal';
	if (vUserId != undefined && vUserId != null) {
		vUrl += "?userId=" + vUserId;
	}
	if (typeof(vIsView) == "boolean" && vIsView) {
		vUrl += "&mandatoryView=true"; 
	}
	
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		if(vUserId==1){
			$.createWindow(vUrl, vTitle, 400, 325, vCallbackFunc);
		}else{
			$.createWindow(vUrl, vTitle, 625, 580, vCallbackFunc);
		}
	} else {
		window.location.href = vUrl;
	}
}

function openUserDialog4LeaveSchool(vAccount, vTitle, vMustOpenAsDialog, vCallbackFunc,vIsView) {
	var vUrl = contextPath + '/usermanage!showInfo4LeaveSchool';
	if (vAccount != undefined && vAccount != null) {
		vUrl += "?account=" + vAccount;
	}
	if (typeof(vIsView) == "boolean" && vIsView) {
		vUrl += "&mandatoryView=true";
	}
	
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		if(vUserId==1){
			$.createWindow(vUrl, vTitle, 400, 325, vCallbackFunc);
		}else{
			$.createWindow(vUrl, vTitle, 625, 580, vCallbackFunc);
		}
	} else {
		window.location.href = vUrl;
	}
}

function openUserDialog4LeaveSchool2(vAccount, vTitle, vMustOpenAsDialog, vCallbackFunc,vIsView) {
	var vUrl = contextPath + '/usermanage!showInfo4LeaveSchool?flag=1';
	if (vAccount != undefined && vAccount != null) {
		vUrl += "&account=" + vAccount;
	}
	if (typeof(vIsView) == "boolean" && vIsView) {
		vUrl += "&mandatoryView=true";
	}
	
	$.createWindow(vUrl, vTitle, 800, 600, vCallbackFunc);
	/*if (vMustOpenAsDialog || isRichClientUIEnabled) {
		if(vUserId==1){
			$.createWindow(vUrl, vTitle, 400, 325, vCallbackFunc);
		}else{
			$.createWindow(vUrl, vTitle, 625, 580, vCallbackFunc);
		}
	} else {
		window.location.href = vUrl;
	}*/
}

function openUserDialog4Thesis(vAccount, vTitle, vMustOpenAsDialog, vCallbackFunc,vIsView) {
	var vUrl = contextPath + '/usermanage!showInfo4Thesis?flag=1';
	if (vAccount != undefined && vAccount != null) {
		vUrl += "&account=" + vAccount;
	}
	if (typeof(vIsView) == "boolean" && vIsView) {
		vUrl += "&mandatoryView=true";
	}
	
	$.createWindow(vUrl, vTitle, 800, 460, vCallbackFunc);
	/*if (vMustOpenAsDialog || isRichClientUIEnabled) {
		if(vUserId==1){
			$.createWindow(vUrl, vTitle, 400, 325, vCallbackFunc);
		}else{
			$.createWindow(vUrl, vTitle, 625, 580, vCallbackFunc);
		}
	} else {
		window.location.href = vUrl;
	}*/
}

/**
 * 打开选择上司弹出框
 */
function openSupervisorSelectDialog(vUserId, vTitle, vCallbackFunc, data1) {
	var vUrl = contextPath + '/usermanage!supervisorSelect';
	if (vUserId != undefined && vUserId != null) {
		vUrl += "?userId=" + vUserId;
	}
	$.createSelectWindow(vUrl, vTitle, 500, 400, vCallbackFunc, data1);
}

/**
 * 打开修改密码弹出框
 */
function openResetPasswordDialog(vUserId, vTitle, vCallbackFunc) {
	var vUrl = contextPath + '/usermanage!resetPassword';
	if (vUserId != undefined && vUserId != null) {
		vUrl += "?userId=" + vUserId;
	}
	$.createWindow(vUrl, vTitle, 500,350, vCallbackFunc);
}

/**
 * 打开选择公司成员弹出框
 */
function openSyscompanyMemberSelectDialog(vUserId, vTitle, vCallbackFunc) {
	openCommonUserSelectDialog(vUserId, vTitle, vCallbackFunc);
}

/**
 * 打开选择公司信息弹出框
 * 
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 */
function openSyscompanyDialog(vParam, vTitle, vMustOpenAsDialog, vCallbackFunc,vIsView) {
	var vUrl = contextPath + '/sysCompany!showInfo';
	if (vParam != undefined && vParam != null) {
		vUrl += "?" + vParam;
	}
	if (typeof(vIsView) == "boolean" && vIsView) {
		vUrl += "&mandatoryView=true"; 
	}
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, vTitle, 640, 375, vCallbackFunc);
	} else {
		window.location.href = vUrl;
	}
}

/**
 * 打开流程定义及部署管理弹出框
 * 
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 */
function openWorkflowTypeDialog(vSeqId, vIsView, vTitle, vMustOpenAsDialog, vCallbackFunc) {
	var vUrl = contextPath + '/workflowType!showInfo';
	if (vSeqId != undefined && vSeqId != null) {
		vUrl += "?seqId=" + vSeqId;
		if (typeof(vIsView) == "boolean" && vIsView) {
			vUrl += "&mandatoryView=true";
		}
	}
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, vTitle,600, 420, vCallbackFunc);
	} else {
		window.location.href = vUrl;
	}
}

/**
 * 打开流程实例管理弹出框
 * 
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 */
function openBillDialog(vSeqId, vIsView, vTitle, vMustOpenAsDialog, vCallbackFunc) {
	var vUrl = contextPath + '/bill!showInfo';
	if (vSeqId != undefined && vSeqId != null) {
		vUrl += "?seqId=" + vSeqId;
		if (typeof(vIsView) == "boolean" && vIsView) {
			vUrl += "&mandatoryView=true";
		}
	}
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, vTitle,600, 420, vCallbackFunc);
	} else {
		window.location.href = vUrl;
	}
}

/**
 * 工作流查看窗口
 * @param vUrl 地址
 * @author ys
 * @date 2013-8-22
 */
function openLoadResourceDialog(vUrl,vMustOpenAsDialog){
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, '查看',900, 500);
	} else {
		window.location.href = vUrl;
	}
}

/**
 * 打开流程应用弹出框
 * 
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 */
function openAppDialog(vDataId,vIsView,vTitle,vMustOpenAsDialog, vCallbackFunc){
	var vUrl = contextPath + "/app!showInfo";
	if (vDataId != undefined && vDataId != null) {
		vUrl += "?appId=" + vDataId;
		if (typeof(vIsView) == "boolean" && vIsView) {
			vUrl += "&mandatoryView=true";
		}
	}
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, vTitle, 800, 400, vCallbackFunc);
	} else {
		window.location.href = vUrl;
	}
}

/**
 * 流程应用-选择流程定义弹出框
 */
function openProcessDefinitionSelectDialog(vDataId, vTitle, vCallbackFunc) {
	var vUrl = contextPath + '/app!toProcessDefinitionSelect';
	if (vDataId != undefined && vDataId != null) {
		vUrl += "?appId=" + vDataId;
	}
	$.createSelectWindow(vUrl, vTitle, 500, 400, vCallbackFunc);
}

/**
 * 流程应用-管理表单弹出框
 * 
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 */
function openAppFormMemberDialog(vDataId, vTitle, vMustOpenAsDialog, vCallbackFunc) {
	var vUrl = contextPath + '/app!toAppFormMember';
	if (vDataId != undefined && vDataId != null) {
		vUrl += "?appId=" + vDataId;
	}
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, vTitle, 600, 450, vCallbackFunc);
	} else {
		window.location.href = vUrl;
	}
}

/**
 * 流程应用-管理表单-选择表单弹出框
 * 
 * @param vDataId 
 * @param vTitle
 * @param vCallbackFunc
 */
function openAppFormSelectDialog(vDataId, vTitle, vCallbackFunc) {
	var vUrl = contextPath + '/app!toAppFormSelect';
	if (vDataId != undefined && vDataId != null) {
		vUrl += "?userId=" + vDataId;
	}
	$.createSelectWindow(vUrl, vTitle, 500, 400, vCallbackFunc);
}

/**
 * 流程应用-设置表单弹出框
 * 
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 */
function openAppFormControlDialog(vDataId, vTitle, vMustOpenAsDialog, vCallbackFunc) {
	var vUrl = contextPath + '/app!toAppFormControl';
	if (vDataId != undefined && vDataId != null) {
		vUrl += "?appId=" + vDataId;
	}
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, vTitle, 600, 450, vCallbackFunc);
	} else {
		window.location.href = vUrl;
	}
}

/**
 * 流程应用-设置表单元素弹出框
 * 
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 */
function openAppFormElementControlDialog(vDataId, vTitle, vMustOpenAsDialog, vCallbackFunc) {
	var vUrl = contextPath + '/app!toAppFormElementControl';
	if (vDataId != undefined && vDataId != null) {
		vUrl += "?appId=" + vDataId;
	}
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, vTitle, 600, 450, vCallbackFunc);
	} else {
		window.location.href = vUrl;
	}
}

/**
 * 流程应用-设置表单菜单项弹出框
 * 
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 */
function openAppFormMenuItemControlDialog(vDataId, vTitle, vMustOpenAsDialog, vCallbackFunc) {
	var vUrl = contextPath + '/app!toAppFormMenuItemControl';
	if (vDataId != undefined && vDataId != null) {
		vUrl += "?appId=" + vDataId;
	}
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, vTitle, 600, 450, vCallbackFunc);
	} else {
		window.location.href = vUrl;
	}
}

/**
 * 打开请假流程管理弹出框
 * 
 * @param vMustOpenAsDialog 是否强制性用弹出框打开。在系统不启动富客户端的模式下，可能会用刷新页面的传统方式打开。
 */
function openTestLeaveDialog(vSeqId, vIsView, vTitle, vMustOpenAsDialog, vCallbackFunc) {
	var vUrl = contextPath + '/testLeave!showInfo';
	if (vSeqId != undefined && vSeqId != null) {
		vUrl += "?seqId=" + vSeqId;
		if (typeof(vIsView) == "boolean" && vIsView) {
			vUrl += "&mandatoryView=true";
		}
	}
	if (vMustOpenAsDialog || isRichClientUIEnabled) {
		$.createWindow(vUrl, vTitle,600, 420, vCallbackFunc);
	} else {
		window.location.href = vUrl;
	}
}


