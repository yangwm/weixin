/**
 *  前端验证框架
 *  @author ys
 *  @create 2012-10-10
 *  @update 2013-2-4
 *  @qq 517757409
 *  @ver 1.3
 */


$.fn.validatebox.defaults.missingMessage = vmissing_message;
$.extend($.fn.validatebox.defaults.rules, {
	/**
	 * 判断是否为整型：比如：validType:int[10,96]
	 */
	int : {
     	validator : function(value, param) {
     		return value>=param[0]&&value<=param[1]&&isInteger(value);     	},
         message : vint_message
     },
	 /**
	  *判断数字是否在范围内：比如：validType:numb[10,18.9]
	  */
	numb : {
		validator : function(value, param) {
     		return value>=param[0]&&value<=param[1];
     	},
         message : numb_message
	}, 
	 /**
	  * 判断是否为浮点型：比如：validType:float[7.9,10]
	  */
	float : {
     	validator : function(value, param) {
     		return value>=param[0]&&value<=param[1]&&isFloat(value);
     	},
         message : vfloat_message
     },
     /**
	  * 判断必须是数字，必须包含小数点，精确到小数点后若干位：比如：validType:floatp[2,2.8,20]
	  */
     floatp : {
      	validator : function(value, param) {
      		var v = value + "";
      		var b = false;
      		if(param[0]==0&&v.lastIndexOf(".")==-1){
      			b = true;
      		}else if(v.lastIndexOf(".")!=-1)
      			b = (v.length - v.lastIndexOf(".")-1)==param[0];
      		return b&&value>=param[1]&&value<=param[2]&&isFloat(value);
      	},
          message : vfloatp_message
      },
	 /**
	  * 判断是否为正数：比如：validType:positive
	  */
     positive : {
     	validator : function(value, param) {
     		return value>0;
     	},
         message : vpositive_message
     },
	 /**
	  * 判断是否为负数：比如：validType:negative
	  */
     negative : {
     	validator : function(value,param){
     		return value<0;
     	},
    	message : vnegative_message
     },
	 /**
	  * 判断是否为邮箱地址：比如：validType:email
	  */
     email:{
         validator : function(value){
         return /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value); 
     	},
     	message : email_message    
     },
	 /**
	  * 判断是否为固定电话：比如：validType:telephone
	  */
     telephone : {
         validator : function(value) {
             return isTelephone(value);
         },
         message : telephone_message
     },
	 /**
	  * 判断是否为手机：比如：validType:mobile
	  */
     mobile : {
         validator : function(value) {
             return isMobile(value);
         },
         message : mobile_message
     },
	 /**
	  * 判断是否为邮政编码：比如：validType:zip
	  */
     zip : {
         validator : function(value) {
             return isZip(value);
         },
         message : zip_message
     },
	 /**
	  * 判断是否为身份证号码：比如：validType:idcard
	  */
    idcard: {
        validator: function (value, param) {
        	//return isIdCard(value);
        	if(isIdCard(value)){
        		return true;
        	}
        	if(isPassport(value)){
        		return true;
        	}
        	if(isHKMacao(value)){
        		return true;
        	}
        	return false;
        },
        message:idcard_message
    },
	/**
	 * 判断是否为中文：比如：validType:chs
	 */
    chs : {
    	 validator: function (value, param) {
            return isCHS(value);
        },
        message:chs_message
    },
	/**
	 * 非中文才能输入：比如：validType:nchs
	 */
    nchs : {
    	 validator: function (value, param) {
            return isNCHS(value);
        },
        message:nchs_message
    },
    /**
	 * 中文，英文字数，数字才能输入：比如：validType:username
	 */
    username : {
    	validator: function (value, param) {
           return isUSERNAME(value);
       },
       message:username_message
   },
	/**
	 * 判断是否为英文：比如：validType:egl
	 */
    egl : {
    	 validator: function (value, param) {
            return isEGL(value);
        },
        message:egl_message
    },

	/**
	 * 判断该输入框是否不等于指定的输入框：比如：validType:vnot_equal['对比Id','错误显示信息','错误显示信息']
	 */
    vnot_equal: {  
        validator: function(value,param,param1){
			return value!=$('#' + param[0]).val();  
        },  
        message: not_equal_message 
    },
	/**
	 * 判断该输入框是否大于指定的输入框：比如：validType:vgreater_than['对比Id','错误显示信息','错误显示信息']
	 */
    vgreater_than: {  
        validator: function(value,param,param1){
			return parseFloat(value)>parseFloat($('#' + param[0]).val());  
        },  
        message: greater_than_message 
    },
	/**
	 * 判断该输入框是否小于指定的输入框：比如：validType:vless_than['对比Id','错误显示信息','错误显示信息']
	 */
    vless_than: {  
        validator: function(value,param,param1){
			return parseFloat(value)<parseFloat($('#' + param[0]).val());  
        },  
        message: less_than_message 
    },
	/**
	 * 判断该输入框是否大于等于指定的输入框：比如：validType:vgreater_than_equal['对比Id','错误显示信息','错误显示信息']
	 */
    vgreater_than_equal: {  
        validator: function(value,param,param1){
			return parseFloat(value)>=parseFloat($('#' + param[0]).val());  
        },  
        message: greater_than_equal_message 
    },
	/**
	 * 判断该输入框是否小于等于指定的输入框：比如：validType:vless_than_equal['对比Id','错误显示信息','错误显示信息']
	 */
    vless_than_equal: {  
        validator: function(value,param,param1){
			return parseFloat(value)<=parseFloat($('#' + param[0]).val());  
        },  
        message: less_than_equal_message 
    },
	/**
	 * 判断该输入框的长度，比如：validType:vsle['最小值','最大值']
	 */
    vsle:{  
        validator: function(value, param){
			return value.length>=param[0]&&value.length<=param[1];
        },  
        message: vsle_message 
    },
	/**
	 * 重复密码的检验：比如：validType:vsle[5,9]
	 */
    vpwd:{
    	 validator: function(value, param){
    	 	return value == $('#' + param[0]).val();
    	 },
    	 message: vpwd_message
    },
    
    vmobileortelephone:{
    	validator: function(value, param){
    	 	return isMobile(value)||isTelephone(value);
    	 },
    	 message: vmobile_telephone
    },
    /**
	 * 英文字数，数字才能输入：比如：validType:password
	 */
    password : {
    	validator: function (value, param) {
           return isPassword(value);
       },
       message:password_message
   },
   /**
    * 英文字数，数字才能输入：比如：validType:account
    */
   account : {
	   validator: function (value, param) {
		   return isAccount(value);
	   },
	   message:account_message
   }
}

);
//是否是整型
function isInteger(str)
{
   var re = new RegExp(/^(-|\+)?\d+$/);
   return re.test(str);
}
//是否是浮点型  
function isFloat(str)
{
   var pos = new RegExp(/^((-|\+)?\d+\.\d+)|0$/);
   return pos.test(str);
}
//是否是固定电话
function isTelephone(str)
{
   var pos = new RegExp(/^0\d{2,3}-?\d{7,8}$/);
   return pos.test(str);
}
//是否是手机
function isMobile(str)
{
   var pos = new RegExp(/^1[3456789]\d{9}$/);
   return pos.test(str);
}

//是否是邮政编码
function isZip(str)
{
   var pos = new RegExp(/^[0-9]\d{5}$/);
   return pos.test(str);
}
//是否是身份证号码
function isIdCard(str){
	var pos = new RegExp(/^\d{15}(\d{2}[A-Za-z0-9])?$/);
	return pos.test(str);
}
//是否是护照号码
function isPassport(str){
	var pos = new RegExp("(^([PSE]{1}\\d{7}|[GS]{1}\\d{8}|[A-Z]{0,3}\\d{6,9})$)");//E字打头的后面不知道要跟几位
	var passport = str.toUpperCase();
	return pos.test(passport);
}
//是否是港澳回乡证号码
function isHKMacao(str){
	var pos = new RegExp(/^[a-zA-Z0-9]{5,12}(\([a-zA-Z0-9]\))?$/);
	//var passport = str.toUpperCase();
	return pos.test(str);
}
//是否是中文
function isCHS(str){
	var pos = new RegExp(/^[\u0391-\uFFE5]+$/);
	return pos.test(str);
}
//是否是英文
function isEGL(str){
	var pos = new RegExp(/^[A-Za-z]+$/);
	return pos.test(str);
}
//是否含有中文
function isNCHS(str){
	var pos = new RegExp(/.*[\u4e00-\u9fa5]+.*$/);
	return !pos.test(str);
}
//只允许输入中文英文字符和数字
function isUSERNAME(str){
	var pos = new RegExp(/^[A-Za-z0-9\u4e00-\u9fa5]+$/);
	return pos.test(str);
}
//只允许输入英文字符和数字
function isPassword(str){
	var pos = new RegExp(/^[A-Za-z0-9_-]+$/);
	return pos.test(str);
}
//英文字符开头和数字结尾
function isAccount(str){
	//var pos = new RegExp(/^[A-Z]{0,3}\d{5,11}[A-Z]*\d{0,9}$/);
	var pos = new RegExp(/^[A-Za-z]*\d{0,11}[A-Za-z]*\d{0,9}$/);
	return pos.test(str);
}