package com.yangwm.wx.web;

import java.util.List;
import com.yangwm.wx.web.bean.Role;

/**
 * 用户登陆安全上下文的默认实现，包括关联用户、用户权限和用户角色等信息。
 * 在用户通过身份验证之后，系统会创建一份登陆安全上下文，并且保存在{@link SecurityContextHolder}中。
 * 
 * @author yangwm
 */
public class SecurityContextImpl implements SecurityContext {

	private static final long serialVersionUID = -7512503500047094555L;

	private String userId;
	/**
	 * 当前用户对象所属的客户代码。
	 */
	private String clientCode;
	/**
	 * 当前用户是否从后台管理登陆系统。
	 */
	private boolean isAdminLogin;
	
	private List<Role> list;
	
	public SecurityContextImpl(String userId,String clientCode) {
		this.userId = userId;
		this.clientCode = clientCode;
	}
	
	/**
	 * 是否为超级管理员
	 */
	@Override
	public boolean isAdministrator() {
		return false;
	}
	
	/**
	 * 是否为普通管理员
	 */
	@Override
	public boolean isAdminNormal() {
		return false;
	}

	/**
	 * 是否为普通用户
	 */
	@Override
	public boolean isGeneralUser() {
		return true;
	}

	@Override
	public String getClientCode() {
		return this.clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	@Override
	public boolean isAdminLogin() {
		return this.isAdminLogin;
	}

	@Override
	public void setAdminLogin(boolean isAdminLogin) {
		this.isAdminLogin = isAdminLogin;
	}

	@Override
	public String getUserId() {
		return userId;
	}
	
	@Override
	public void setUserId(String userId) {
		this.userId=userId;
	}

	@Override
	public List<Role> getRoleList() {
		return list;
	}

	@Override
	public void setRoleList(List<Role> list) {
		this.list=list;
	}
}
