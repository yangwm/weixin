package com.yangwm.wx.web;

/**
 * {@code SecurityContext}的持有者，负责把经过身份验证后生成的{@code SecurityContext}关联到当前执行线程。
 * 
 * @author yangwm
 */
public class SecurityContextHolder {
	
    public static final String MODE_THREADLOCAL = "MODE_THREADLOCAL";
    public static final String MODE_HTTP_SESSION = "MODE_HTTP_SESSION";
    public static final String MODE_GLOBAL = "MODE_GLOBAL";
    public static final String SYSTEM_PROPERTY = "cynthia.security.strategy";
    private static String strategyName = System.getProperty(SYSTEM_PROPERTY);
    private static SecurityContextHolderStrategy strategy;

    /**
     * Explicitly clears the context value from the current thread.
     */
    public static void clearContext(String target) {
        if(strategy!=null){
    		strategy.clearContext(target);
    	}
    }

    /**
     * Obtain the current <code>SecurityContext</code>.
     *
     * @return the security context (never <code>null</code>)
     */
    public static SecurityContext getContext(String target) {
    	if(strategy==null){
    		return null;
    	}
        return strategy.getContext(target);
    }

    private static void initialize() {
        if ((strategyName == null) || "".equals(strategyName)) {
            // Set default
            strategyName = MODE_HTTP_SESSION;
        }

        if (strategyName.equals(MODE_THREADLOCAL)) {
        	strategy = new ThreadLocalSecurityContextHolderStrategy();
        }else if (strategyName.equals(MODE_HTTP_SESSION)) {
            strategy = new HttpSessionSecurityContextHolderStrategy();
        } else {
        	throw new RuntimeException("Not appropriate SecurityContextHolder found.");
        }
    }

    /**
     * Associates a new <code>SecurityContext</code> with the current thread of execution.
     *
     * @param context the new <code>SecurityContext</code> (may not be <code>null</code>)
     */
    public static void setContext(SecurityContext context) {
    	initialize();
    	strategy.setContext(context);
    }

    /**
     * Changes the preferred strategy. Do <em>NOT</em> call this method more than once for a given JVM, as it
     * will re-initialize the strategy and adversely affect any existing threads using the old strategy.
     *
     * @param strategyName the fully qualified class name of the strategy that should be used.
     */
    public static void setStrategyName(String strategyName) {
        SecurityContextHolder.strategyName = strategyName;
        //initialize();
    }

    /**
     * Allows retrieval of the context strategy. See SEC-1188.
     *
     * @return the configured strategy for storing the security context.
     */
    public static SecurityContextHolderStrategy getContextHolderStrategy() {
        return strategy;
    }
}
