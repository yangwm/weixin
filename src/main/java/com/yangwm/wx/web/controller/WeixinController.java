package com.yangwm.wx.web.controller;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yangwm.wx.web.base.BaseController;
import com.yangwm.wx.web.bean.Users;
import com.yangwm.wx.web.service.UserService;
import com.yangwm.wx.web.util.WeixinCache;
import com.yangwm.wx.web.util.WeixinUtils;

/**
 * 微信扫码登录
 * @author yangwm
 * @date 2021-11-30
 */
@Controller
@RequestMapping(value="index")
public class WeixinController extends BaseController{
	private static Logger logger=LoggerFactory.getLogger(WeixinController.class);
	
	private static final String CHART_SET="UTF-8";
	private static final String QRCODE_TYPE="QRCODE_LOGIN";
	private static final String TOKEN="weixin-yangwm";
	
	@Resource
	private UserService userService;

	/** 通过微信公众平台生成带参数的二维码
	 * @see 官网说明文档：https://developers.weixin.qq.com/doc/offiaccount/Account_Management/Generating_a_Parametric_QR_Code.html
	 */
	@RequestMapping(value="/wxScanQRCode",method={RequestMethod.GET,RequestMethod.POST})
	public void wxScanQRCode(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//正式环境中，accessToken应存于缓存中，每次从缓存中提取，如果缓存中提取不到，再调用公众号接口提取accessToken
		String accessToken = WeixinUtils.getAccessToken();
		String cacheKey = request.getParameter("key");
		logger.debug("当前用户缓存key:{}", cacheKey);
		WeixinCache.put(cacheKey, "none");
		WeixinCache.put(cacheKey + "_done", false);
		//正式环境中，qrCodeTicket应存于缓存中，每次从缓存中提取，如果缓存中提取不到，再调用公众号接口提取qrCodeTicket
		String qrCodeTicket=WeixinUtils.getQrCodeTiket(accessToken, QRCODE_TYPE, cacheKey);
		InputStream is = WeixinUtils.getQrCodeStream(qrCodeTicket);
		if(is != null) {
			BufferedInputStream bis=new BufferedInputStream(is);
			response.setContentType("image/jpeg; charset=utf-8");
			OutputStream os = response.getOutputStream();
			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = bis.read(buffer)) != -1) {
				os.write(buffer, 0, len);
			}
			bis.close();
			os.flush();
		}
	}

	/** 微信授权登录-轮询 */
	@ResponseBody
	@RequestMapping(value="/wxScanQRCodePolling", method=RequestMethod.POST)
	public Map<String,Object> wxScanQRCodePolling(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String cacheKey=request.getParameter("key");
		logger.debug("登录轮询读取缓存key:{}", cacheKey);
		Boolean cacheDone=(Boolean) WeixinCache.get(cacheKey + "_done");
		//response.setContentType("application/json;charset=utf-8");
		String rquestBody=WeixinUtils.InPutstream2String(request.getInputStream(), CHART_SET);
		logger.debug("获取到请求正文:{}",rquestBody);
		logger.debug("是否扫码成功:{}", cacheDone);
		Map<String,Object> dataMap=new HashMap<String, Object>();
		if (cacheDone != null && cacheDone) {
			JSONObject bindUser=(JSONObject) WeixinCache.get(cacheKey);
			dataMap.put("errcode", 0);
			dataMap.put("errmsg", "ok");
			dataMap.put("binduser", bindUser);
			WeixinCache.remove(cacheKey);
			WeixinCache.remove(cacheKey + "_done");
			logger.debug("已移除缓存数据，key：{}", cacheKey);
			response.sendRedirect(request.getContextPath()+"/wxSuccess.html");
			return dataMap;
		}
		
		dataMap.put("errcode", 100);
		dataMap.put("errmsg", "用户还未扫码");
		return dataMap;
	}

	/** 校验微信公众平台接口签名信息 */
	@RequestMapping(value="/wxEventHandler", method={ RequestMethod.GET })
	public void wxEventHandler(HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.debug("微信在配置服务器传递验证参数");
		Map<String, String[]> reqParam=request.getParameterMap();
		for (String key : reqParam.keySet()) {
			logger.debug(" {}={}", key, reqParam.get(key));
		}

		String signature=request.getParameter("signature");
		String echostr=request.getParameter("echostr");
		String timestamp=request.getParameter("timestamp");
		String nonce=request.getParameter("nonce");

		String buildSign=WeixinUtils.getSignature(TOKEN, timestamp, nonce);

		logger.debug("服务器生成签名信息:{}", buildSign);
		if (buildSign.equals(signature)) {
			response.getWriter().write(echostr);
			logger.debug("服务生成签名与微信服务器生成签名一致，校验成功");
			return;
		}
	}
	
	/** 处理微信公众平台事件 */
	@RequestMapping(value="/wxEventHandler", method={ RequestMethod.POST })
	public void wxEventHandlerPost(HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			SAXReader reader=new SAXReader();
			Document doc=reader.read(request.getInputStream());
			Element root=doc.getRootElement();
			String openId=root.element("FromUserName").getText();
			logger.debug("获取到扫码用户openid:{}", openId);
			String msgTypeStr=root.element("MsgType").getText();
			if ("event".equals(msgTypeStr)) {
				String eventStr=root.element("Event").getText();
				logger.debug("获取到event类型:{}", eventStr);
				if ("SCAN".equals(eventStr)) {
					String eventKeyStr=root.element("EventKey").getText();
					logger.debug("获取到扫码场景值:{}", eventKeyStr);
					if (eventKeyStr.indexOf(QRCODE_TYPE) == 0) {
						String cacheKey=eventKeyStr.split("#")[1];
						scanLogin(openId, cacheKey);
					}
				}
			}
			if ("text".equals(msgTypeStr)) {
				String contentStr=root.element("Content").getText();
				logger.debug("用户发送信息:{}", contentStr);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("微信调用服务后台出现错误", e.getCause());
		}
	}
	
	/** 处理微信公众平台wxBind事件 */
	@ResponseBody
	@RequestMapping(value="/wxBind", method={RequestMethod.GET,RequestMethod.POST })
	public Map<String,Object> wxBind(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String key = request.getParameter("key");
		String userName = request.getParameter("u");
		String password = request.getParameter("p");
		
		//  获取openID
		String openId = (String) WeixinCache.get(key);
		logger.debug("获取到缓存openid {}", openId);
		Users u=new Users();
		u.setAccount(userName);
		Users user = findUser(u);
		if (user == null) {
			user = new Users();
			user.setAccount(userName);
			user.setOpenid(openId);
			saveBindData(user);
		}else {
			user.setOpenid(openId);
			saveBindData(user);
		}
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("errcode", 0);
		map.put("errmsg", "绑定成功");
		//response.getWriter().print(JSON.toJSONString(map));
		return map;
	}
	
	private Users findUser(Users user) throws IOException {
		return userService.findUser(user);
	}
	
	private JSONObject findUserByNameAndPwd(String name, String pwd) throws IOException {
		File file = ResourceUtils.getFile("classpath:userdata.json");
		String serverUserJson =FileUtils.readFileToString(file,CHART_SET);
		logger.debug("读取到服务端存储JSON文件 {}", serverUserJson);
		JSONArray users = JSONObject.parseArray(serverUserJson);
		for (int i = 0, len = users.size(); i < len; i++) {
			JSONObject user = users.getJSONObject(i);
			if (name.equals(user.getString("userName")) && pwd.equals(user.getString("password"))) {
				return user;
			}
		}
		return null;
	}

	private void saveBindData(Users user) throws IOException {
		userService.saveUser(user);
	}
	
	private void saveBindData_bak(JSONObject user) throws IOException {
		String path = ResourceUtils.getURL("classpath:userdata.json").getPath();
		logger.debug("数据文件保存路径:{}", path);
		FileUtils.writeStringToFile(new File(path),"["+user.toJSONString()+"]", CHART_SET);
//		PrintStream ps = new PrintStream(path);
//		ps.print("[" + user.toJSONString() + "]");
//		ps.flush();
//		ps.close();
	}
	
	/** 网页授权登录的回调函数 */
	@RequestMapping(value="/wxCallback", method={ RequestMethod.GET, RequestMethod.POST })
	public void wxCallback(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String code=request.getParameter("code");
		String state=request.getParameter("state");
		logger.debug("获取到微信回传参数code:{},state:{}", code, state);
		JSONObject obj=WeixinUtils.getWebAuthTokenInfo(code);
		if (obj != null && !obj.containsKey("errcode")) {
			String openId=obj.getString("openid");
			logger.debug("opeind:{}", openId);
			Users u=new Users();
			u.setOpenid(openId);
			Users user=findUser(u);
			if (user == null) {
				//用户未绑定 将openid存入缓存方便下一步绑定用户
				WeixinCache.put(state, openId);
				response.sendRedirect("wxBind.html?key=" + state);
				return;
			}
			WeixinCache.put(state, user);
			WeixinCache.put(state + "_done", true);
			logger.debug("已将缓存标志[key]:{}设置为true", state + "_done");
			logger.debug("已更新缓存[key]:{}", state);

			//跳转到登录成功的提示页
//			response.setCharacterEncoding("GBK");
//			response.getWriter().print("扫码成功，已成功登录系统");
			response.sendRedirect(request.getContextPath()+"/wxSuccess.html");
		}
	}
	
	/** 微信公众号网页授权登录 */
	@RequestMapping(value="/wxWebQRCode", method={ RequestMethod.GET, RequestMethod.POST })
	public void wxWebQRCode(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String cacheKey=request.getParameter("key");
		logger.debug("当前用户缓存key:{}", cacheKey);

		BufferedImage bImg=WeixinUtils.buildWebAuthUrlQrCode("http://pro.vaiwan.com/wx/index/wxCallback",cacheKey);
		if (bImg != null) {
			response.setContentType("image/png; charset=utf-8");
			OutputStream os=response.getOutputStream();
			ImageIO.write(bImg, "png", os);
			os.flush();
		}
	}
	
	private void scanLogin(String openId, String cacheKey) throws IOException {
		JSONObject user=findUserByOpenId(openId);
		if (user == null) {
			// 发送消息让用户绑定账号
			logger.debug("用户还未绑定微信，正在发送邀请绑定微信消息");
			WeixinUtils.sendTempalteMsg(WeixinUtils.getAccessToken(), openId,
					"9-kDp8mh4EX-MiXNyXt-5QYn9tUhP08p8C8Au2-XEC4",
					"http://pro.vaiwan.com/wx/wxBind.html?key=" + cacheKey, null);
			WeixinCache.put(cacheKey, openId);
			return;
		}
		// 更新缓存
		WeixinCache.put(cacheKey, user);
		WeixinCache.put(cacheKey + "_done", true);
		logger.debug("已将缓存标志[key]:{}设置为true", cacheKey + "_done");
		logger.debug("已更新缓存[key]:{}", cacheKey);
		logger.debug("已发送登录成功微信消息");
		WeixinUtils.sendTempalteMsg(WeixinUtils.getAccessToken(), openId, "nXQZOvWtz-PZvhW7UP77x0VNm4aGnUW-Ugiylg2WI4c",null, null);
	}

	private JSONObject findUserByOpenId(String openId) throws IOException {
//		ClassPathResource resource=new ClassPathResource("userdata.json");
//		logger.debug(resource.getFile().getPath());
		String path=ResourceUtils.getURL("classpath:userdata.json").getPath();
		String serverUserJson=FileUtils.readFileToString(new File(path),CHART_SET);
		logger.debug("JSON:{}", serverUserJson);
		JSONArray users=JSONObject.parseArray(serverUserJson);
		for (int i=0, len=users.size(); i < len; i++) {
			JSONObject user=users.getJSONObject(i);
			if (openId.equals(user.getString("openId"))) {
				return user;
			}
		}
		return null;
	}
}