package com.yangwm.wx.web;

/**
 * <pre>
 * 身份验证管理器，统一对外提供系统的身份验证功能服务。
 * 身份验证功能封装在{@code AuthenticationProvider}中。
 * </pre>
 * 
 * @author Administrator
 * @see AuthenticationProvider
 * @see DaoAuthenticationService
 * @see SecurityContextHolder
 */
public abstract class AuthenticationManager {
	
	/**
	 * 是否通过了验证。
	 * 
	 * @param result 验证的返回值
	 * @return
	 */
	public static boolean passAuthentication(int result) {
		return (result == 0);
	}

	/**
	 * 当前用户是否已经通过身份验证
	 * 
	 * @return
	 */
	public static boolean hasCurrentUserAuthenticated() {
		return (SecurityContextHolder.getContext(null) != null);
	}
	
	/**
	 * 用户注销之后，清除登陆安全上下文。
	 */
	public void clearSecurityContext() {
		SecurityContextHolder.clearContext(null);
	}
}
