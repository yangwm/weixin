package com.yangwm.wx.web.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

//@Entity(name="User")
public class UumsUser implements Serializable {
	private static final long serialVersionUID = 1L;
	
	// @Id
	// @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer userId;

	private String account;
	private String idCard;
	private String userName;
	private String employeeId;
	private String cardNo;
	private String password;
	private String status;
	private String userType;
	private String sex;
	private String defaultLocale;
	private String birthday;
	private Integer companyId;
	private Integer supervisor;
	private Integer titleId;
	private String email;
	private String homeAddr;
	private String contactAddr;
	private String zipCode;
	private String mobileNumber;
	private String phone;
	private String fax;
	private String remark;
	private Integer creatorId;
	private String createTime;
	private Integer lastUpdateUser;
	private String lastUpdateTime;
	private String major;//所学专业或所在班级
	private String expiryDate;//离校/毕业时间
	
	private Integer campus;//常驻校区，0:北校，1：南校
	private String grade;//所在年级
	private String classes;//所在班级
	private String alephId;//读者id(aleph系统中读者id)
	private String aleph1delinq;//读者全局限制(aleph系统中读者全局限制)
	private String aleph2delinq;//读者本地限制(aleph系统中读者本地限制)
	
	private String microblog;//微博
    private String qq;//qq
    private String weixin;//微信

	@Transient
	private String companyName;
	@Transient
	private String titleName;//用户类型：本科生/研究生/教职工
	@Transient
	private String projectIds;//关联项目id
	@Transient
	private Integer trackIntervals;//每隔N天可换方案
	private Integer trackType;//0刷脸；1刷卡
	
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getDefaultLocale() {
		return defaultLocale;
	}

	public void setDefaultLocale(String defaultLocale) {
		this.defaultLocale = defaultLocale;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public Integer getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Integer supervisor) {
		this.supervisor = supervisor;
	}

	public Integer getTitleId() {
		return titleId;
	}

	public void setTitleId(Integer titleId) {
		this.titleId = titleId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHomeAddr() {
		return homeAddr;
	}

	public void setHomeAddr(String homeAddr) {
		this.homeAddr = homeAddr;
	}

	public String getContactAddr() {
		return contactAddr;
	}

	public void setContactAddr(String contactAddr) {
		this.contactAddr = contactAddr;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(Integer creatorId) {
		this.creatorId = creatorId;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Integer getLastUpdateUser() {
		return lastUpdateUser;
	}

	public void setLastUpdateUser(Integer lastUpdateUser) {
		this.lastUpdateUser = lastUpdateUser;
	}

	public String getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	public String getProjectIds() {
		return projectIds;
	}

	public void setProjectIds(String projectIds) {
		this.projectIds = projectIds;
	}

	public Integer getTrackIntervals() {
		return trackIntervals;
	}

	public void setTrackIntervals(Integer trackIntervals) {
		this.trackIntervals = trackIntervals;
	}

	public Integer getTrackType() {
		return trackType;
	}

	public void setTrackType(Integer trackType) {
		this.trackType = trackType;
	}

	public Integer getCampus() {
		return campus;
	}

	public void setCampus(Integer campus) {
		this.campus = campus;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getClasses() {
		return classes;
	}

	public void setClasses(String classes) {
		this.classes = classes;
	}

	public String getAlephId() {
		return alephId;
	}

	public void setAlephId(String alephId) {
		this.alephId = alephId;
	}

	public String getAleph1delinq() {
		return aleph1delinq;
	}

	public void setAleph1delinq(String aleph1delinq) {
		this.aleph1delinq = aleph1delinq;
	}

	public String getAleph2delinq() {
		return aleph2delinq;
	}

	public void setAleph2delinq(String aleph2delinq) {
		this.aleph2delinq = aleph2delinq;
	}

	public String getMicroblog() {
		return microblog;
	}

	public void setMicroblog(String microblog) {
		this.microblog = microblog;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getWeixin() {
		return weixin;
	}

	public void setWeixin(String weixin) {
		this.weixin = weixin;
	}
}