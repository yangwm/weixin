package com.yangwm.wx.web;

/**
 * 存储跟线程相关的{@link SecurityContext}的策略。
 * 
 * @author Administrator
 * @see HttpSessionSecurityContextHolderStrategy
 * @see ThreadLocalSecurityContextHolderStrategy
 */
public interface SecurityContextHolderStrategy {
	/**
	 * 清楚当前的登陆安全上下文
	 * @param target 前缀+Constant#LOGIN_SESSION_APP/LOGIN_SESSION_WEB+userId
	 */
    void clearContext(String target);

    /**
     * 返回当前的登陆安全上下文
     * @param target 前缀+Constant#LOGIN_SESSION_APP/LOGIN_SESSION_WEB+userId
     * @return
     */
    SecurityContext getContext(String target);

    /**
     * 设置当前登陆安全上下文。子类应该保证该context不能为空。
     * 
     * @param context
     */
    void setContext(SecurityContext context);
}
