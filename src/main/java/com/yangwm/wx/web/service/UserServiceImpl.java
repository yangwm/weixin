package com.yangwm.wx.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import javax.transaction.Transactional;
import com.yangwm.wx.web.bean.Users;
import com.yangwm.wx.web.dao.UserDao;

@Service("userService")
//@Transactional
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao userDao;

	@Override
	public Users findUser(Users user) {
		return userDao.findUser(user);
	}

	@Override
	public Users findUser(Integer userId) {
		return userDao.findUser(userId);
	}

	@Override
	public Users findUser(String account) {
		return userDao.findUser(account);
	}

	@Override
	public List<Users> queryUsers() {
		return userDao.queryUsers();
	}

	@Override
	public List<Users> queryUsers(Users user) {
		return userDao.queryUsers(user);
	}

	@Override
	public Users saveUser(Users user) {
		return userDao.saveUser(user);
	}

	@Override
	public int updateUserPwd(Users user) {
		return userDao.updateUserPwd(user);
	}

	@Override
	public int updateUserScore(Users user) {
		return userDao.updateUserScore(user);
	}

	@Override
	public void deleteUser(int id) {
		userDao.deleteUser(id);
	}
}