package com.yangwm.wx.web.service;

import java.util.List;

import com.yangwm.wx.web.bean.Users;

public interface UserService {
	public Users findUser(Users user);

	public Users findUser(String account);

	public Users findUser(Integer userId);

	public List<Users> queryUsers();

	public Users saveUser(Users user);

	public int updateUserPwd(Users user);

	public int updateUserScore(Users user);

	public void deleteUser(int id);

	public List<Users> queryUsers(Users user);
}