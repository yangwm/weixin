package com.yangwm.wx.web;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;

import com.yangwm.wx.web.util.PropertiesUtil;

/**
 * @author yangwm
 */
public class AuthenticationFilter extends OncePerRequestFilter {
	
	protected void initFilterBean() throws ServletException {
		super.initFilterBean();
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request,HttpServletResponse response, FilterChain filterChain)throws ServletException,IOException {
		// Make sure the session is created here
		request.getSession(true);
		PropertiesUtil propertiesUtil=PropertiesUtil.getInstance();
		if(!RequestUtil.isStaticResource(request) && !AuthenticationManager.hasCurrentUserAuthenticated() && !RequestUtil.isAuthencationIgnored(request)) {
			if(propertiesUtil.isSsoLoginStrategy()){
				RequestUtil.sendRedirect4SsoLogin(request, response);
			}else{
				RequestUtil.sendRedirect2LoginPage(request, response);
			}
			return;
		}
		
		filterChain.doFilter(request, response);
	}

}
