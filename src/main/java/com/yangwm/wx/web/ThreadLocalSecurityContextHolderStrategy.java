package com.yangwm.wx.web;

/**
 * 基于ThreadLocal的{@link SecurityContextHolderStrategy}实现。
 * 
 * @author yangwm
 */
final class ThreadLocalSecurityContextHolderStrategy implements SecurityContextHolderStrategy {
	
    private static final ThreadLocal<SecurityContext> contextHolder = new ThreadLocal<SecurityContext>();

    public void clearContext(String target) {
        contextHolder.remove();
    }

    public SecurityContext getContext(String target) {
        return contextHolder.get();
    }

    public void setContext(SecurityContext context) {
        contextHolder.set(context);
    }
}
