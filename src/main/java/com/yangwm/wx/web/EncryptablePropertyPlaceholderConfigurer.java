package com.yangwm.wx.web;

import java.util.Base64;
import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

public class EncryptablePropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {
	
	@Override
	protected void processProperties(ConfigurableListableBeanFactory beanFactoryToProcess,Properties props)throws BeansException {
		String userName=props.getProperty("datasource.mysql.user");
		String password=props.getProperty("datasource.mysql.password");
		if (userName != null) {
			props.setProperty("datasource.mysql.user",decrypt(userName));
		}
		if (password != null) {
			props.setProperty("datasource.mysql.password",decrypt(password));
		}
		super.processProperties(beanFactoryToProcess,props);
	}
	
	/** 加密 */
	public String encrypt(String s) {
		return variance(s,1566,true);
	}
	
	/** 解密 */
	public String decrypt(String s) {
		return variance(s,1566,false);
	}
	
	private String variance(final String s,int key,final boolean isEncrypt) {
		final int seedA=3467;
		final int seedB=1239;
		byte ps,pr;
		try {
			byte[] src=isEncrypt ? s.getBytes() : Base64.getDecoder().decode(s);
			byte[] buf=new byte[src.length];
			for (int i=0; i < src.length; i++) {
				ps=src[i];
				pr=(byte) (ps ^ (key >>> 8));
				buf[i]=pr;
				if (isEncrypt){
					key=(pr+key)*seedA+seedB;
				}else{
					key=(ps+key)*seedA+seedB;
				}
			}
			return isEncrypt ? Base64.getEncoder().encodeToString(buf) : new String(buf);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}