package com.yangwm.wx.web;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.ObjectUtils;
import com.yangwm.wx.web.base.Constant;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * 基于HTTP Session的{@link SecurityContextHolderStrategy}实现。
 * 
 * @author yangwm
 *
 */
final class HttpSessionSecurityContextHolderStrategy implements SecurityContextHolderStrategy {

	static final String CYNTHIA_SECURITY_CONTEXT = "cynthia_security_context";
	
	@Override
	public void clearContext(String target) {
		this.getSession().removeAttribute(CYNTHIA_SECURITY_CONTEXT+ObjectUtils.toString(target));
	}

	@Override
	public SecurityContext getContext(String target) {
		return (SecurityContext) this.getSession().getAttribute(CYNTHIA_SECURITY_CONTEXT+ObjectUtils.toString(target));
	}

	@Override
	public void setContext(SecurityContext context) {
		String clientCode=ObjectUtils.toString(context.getClientCode());
		if(Constant.LOGIN_SESSION_APP.equals(clientCode)){
			this.getSession().setAttribute(CYNTHIA_SECURITY_CONTEXT+ObjectUtils.toString(context.getClientCode())+ObjectUtils.toString(context.getUserId()), context);
		}else{
			this.getSession().setAttribute(CYNTHIA_SECURITY_CONTEXT, context);
		}
	}
	
	private HttpSession getSession() {
		return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getSession();
//		return ((ServletWebRequest) RequestContextHolder.getRequestAttributes()).getRequest().getSession(false);
	}

}
