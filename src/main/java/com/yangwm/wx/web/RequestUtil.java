package com.yangwm.wx.web;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.yangwm.wx.web.util.PropertiesUtil;

/**
 * @author yangwm
 */
public abstract class RequestUtil {
	private static Collection<String> dynamicResourceUrlExtensions = new HashSet<String>();
	static {
		dynamicResourceUrlExtensions.add("do");
		dynamicResourceUrlExtensions.add("action");
		dynamicResourceUrlExtensions.add("html");
		dynamicResourceUrlExtensions.add("htm");
		dynamicResourceUrlExtensions.add("jsp");
	}
	
	/**
	 * 是否不需要身份认证
	 * @param request
	 * @return
	 */
	public static boolean isAuthencationIgnored(HttpServletRequest request) {
		/*String uri = getRequestURI(request);
		if (uri.lastIndexOf(".") == -1) {
			return true;
		}
		
		String extension = uri.substring(uri.indexOf(".") + 1);
		if (dynamicResourceUrlExtensions.contains(extension)) {
			return true;
		}
		return false;*/
		String uri = extractIgnoreUri(request, true);
		PropertiesUtil util=PropertiesUtil.getInstance();
		String ignoreUris=util.getProperty("auth.ignore.uris");
		return (ignoreUris != null && ignoreUris.contains(uri));
	}
	
	/**
	 * 是否为静态资源请求
	 * 
	 * @param request
	 * @return
	 */
	public static boolean isStaticResource(HttpServletRequest request) {
		String uri = getRequestURI(request);
		if (uri.lastIndexOf(".") == -1) {
			return false;
		}
		
		String extension = uri.substring(uri.indexOf(".") + 1);
		if (dynamicResourceUrlExtensions.contains(extension)) {
			return false;
		}
		return true;
	}
	
	/**
	 * @param request
	 * @param dropUriExtension
	 * @return
	 */
	private static String extractIgnoreUri(HttpServletRequest request, boolean dropUriExtension) {
		String uri = getRequestURI(request);
		String result = uri;
		String contextPath = request.getContextPath();
		// drop context path
		if (!StringUtils.isEmpty(contextPath) && uri.indexOf(contextPath) > -1) {
			result = uri.substring(uri.indexOf(contextPath) + contextPath.length());
		}
		
		// drop uri extension
		if (dropUriExtension) {
			if (result.indexOf(".") > -1) {
				result = result.substring(0, result.indexOf("."));
			}
		}
		
		return result;
	}
	
	/**
	 * 重定向到登陆页面
	 * 
	 * @param req
	 * @param resp
	 * @throws IOException
	 */
	public static void sendRedirect2LoginPage(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		StringBuilder sb = new StringBuilder(req.getContextPath()).append("/toLogin");
		// 用于控制父页面跳转
		String windowId = req.getParameter("gdis-jq-window-id");
		if (StringUtils.isNotEmpty(windowId)) {
			sb.append("?gdis-jq-window-id=").append(windowId);
		}
		String tabId = req.getParameter("gdis-jq-tab-id");
		if (StringUtils.isNotEmpty(tabId)) {
			sb.append(sb.indexOf("?") == -1 ? "?" : "&").append("gdis-jq-tab-id=").append(tabId);
		}
		sendRedirect(req, resp, sb.toString());
		/*StringBuilder sb = new StringBuilder(req.getContextPath()).append("/syslogin.do");
		sendRedirect(req, resp, sb.toString());*/
	}

	/**
	 * 重定向到特定页面
	 * 
	 * @param req
	 * @param resp
	 * @param page
	 * @throws IOException
	 */
	public static void sendRedirect(HttpServletRequest req, HttpServletResponse resp,String page) throws IOException {
		if (!resp.isCommitted()) {
			resp.sendRedirect(page);
		}
	}
	
	/**
	 * 生成表格翻页URL。
	 * 
	 * @param req
	 * @param resp
	 * @param pageNumber 该链接对应的页码
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public static String genPaginationLink(HttpServletRequest req, HttpServletResponse resp, int pageNumber) {
		StringBuilder url = new StringBuilder();
		//url.append(req.getRequestURL());
		url.append(req.getRequestURI());
		
		boolean isFirstParameter = true;
		for (Map.Entry<String, String[]> entry : req.getParameterMap().entrySet()) {
			String key = entry.getKey();
			String[] values = entry.getValue();
			if (!"page".equals(key)) {
				if (values != null) {
					if (values.length == 1) {
						appendURLParameter(url, key, values[0], isFirstParameter);
					} else {
						for (String value : values) {
							appendURLParameter(url, key, value, isFirstParameter);
						}
					}
					
					isFirstParameter = false;
				}
			} else {
				appendURLParameter(url, key, String.valueOf(pageNumber), isFirstParameter);
				isFirstParameter = false;
			}
		}
		
		return resp.encodeURL(url.toString());
	}
	
	private static void appendURLParameter(StringBuilder url, String parameter, String value, boolean isFirstParameter) {
		url.append(isFirstParameter ? "?" : "&").append(parameter).append("=").append(encodeUrlParaValue(value));
	}
	
	/**
	 * 对HTTP请求参数值进行编码。
	 * 
	 * @param value
	 * @return
	 */
	public static String encodeUrlParaValue(String value) {
		if (StringUtils.isEmpty(value)) {
			return value;
		}
		
		try {
			return URLEncoder.encode(value,"utf-8");
		} catch (UnsupportedEncodingException e) {
			return URLEncoder.encode(value);
		}
	}
	
	/**
     * 返回请求的requestURI。例如：
     * <pre class="codeHtml">
     * <span class="blue">http://www.mycorp.com/banking/secure/login.htm</span>  ->  <span class="red">/banking/secure/login.htm</span> </pre>
     *
     * @param request 页面请求
     * @return 请求的requestURI
     */
    public static String getRequestURI(HttpServletRequest request) {
        String requestURI = (String) request.getAttribute("javax.servlet.include.request_uri");

        if (requestURI == null) {
            requestURI = request.getRequestURI();
        }
        /*
        if (requestURI != null && requestURI.endsWith(".jsp")) {
            requestURI = StringUtils.replace(requestURI, ".jsp", ".html");
        }
		*/
        return requestURI;
    }
    
    /**
     * 返回客户端ip地址
     * 
     * @param request
     * @return
     */
    public static String getRemoteAddr(HttpServletRequest request) {
	    String ip = request.getHeader("X-Forwarded-For");
	    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	        ip = request.getHeader("Proxy-Client-IP");
	    }
	    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	        ip = request.getHeader("WL-Proxy-Client-IP");
	    }
	    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	        ip = request.getHeader("HTTP_CLIENT_IP");
	    }
	    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	        ip = request.getHeader("HTTP_X_FORWARDED_FOR");
	    }
	    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	        ip = request.getRemoteAddr();
	    }
	    return ip;
    }
    
    /**
     * 返回客户端的主机名称。
     * 
     * @param request
     * @return
     */
    public static String getRemoteHost(HttpServletRequest request) {
    	return request.getRemoteHost();
    }
    
    /**
     * 返回客户端的主机名称。
     * 
     * @param request
     * @return
     */
    public static boolean isFormLogin(HttpServletRequest request) {
    	String value=(String)request.getSession().getAttribute("logion");
//    	String logion = this.getSessionStr(request, "logion");
//    	String sessionId=WebContextUtil.getSessionId();
    	if("logion".equals(value)){
    		return true;
    	}
    	return false;
    }
    
    /**
	 * 重定向到SSO登陆页面
	 */
    public static void sendRedirect4SsoLogin(HttpServletRequest request,HttpServletResponse response) {
    	PropertiesUtil propertiesUtil=PropertiesUtil.getInstance();
    	String url = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
		try {
			url=URLEncoder.encode(url,"utf8");
			response.sendRedirect(propertiesUtil.getSsoUrl()+"login?service="+url);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    /**
	 * 重定向到SSO登出页面
	 */
    public static void sendRedirect4SsoLogout(HttpServletRequest request,HttpServletResponse response) {
    	PropertiesUtil propertiesUtil=PropertiesUtil.getInstance();
    	String url = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
    	try {
    		url=URLEncoder.encode(url,"utf8");
    		response.sendRedirect(propertiesUtil.getSsoUrl()+"logout?service="+url);
    	} catch (UnsupportedEncodingException e) {
    		e.printStackTrace();
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
    }
}
