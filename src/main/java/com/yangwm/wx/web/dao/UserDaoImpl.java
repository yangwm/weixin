package com.yangwm.wx.web.dao;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.yangwm.wx.web.bean.Users;
import com.yangwm.wx.web.service.UserService;

@Repository("userDao")
public class UserDaoImpl implements UserDao {
	private static Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

	@Resource
	private UserService userService;
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	protected final Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public Users findUser(Users user) {
		StringBuffer sql = new StringBuffer("select * from wx_user where 1=1");
		if (user.getUserId() != null) {
			sql.append(" and userid=").append(user.getUserId());
		}
		if (StringUtils.isNotEmpty(user.getAccount())) {
			sql.append(" and account='").append(user.getAccount()).append("'");
		}
		if (StringUtils.isNotEmpty(user.getOpenid())) {
			sql.append(" and openid='").append(user.getOpenid()).append("'");
		}
		RowMapper<Users> rowMapper = new BeanPropertyRowMapper<Users>(Users.class);
		List<Users> list = jdbcTemplate.query(sql.toString(), rowMapper);
		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public Users findUser(String account) {
		if (StringUtils.isEmpty(account)) {
			return null;
		}
		StringBuffer sql = new StringBuffer("select * from users where 1=1");
		sql.append(" and account='").append(account).append("'");
		// sql.append(" order by type ");
		RowMapper<Users> rowMapper = new BeanPropertyRowMapper<Users>(Users.class);
		List<Users> list = jdbcTemplate.query(sql.toString(), rowMapper);
		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public Users findUser(Integer userId) {
		if (userId == null) {
			return null;
		}
		StringBuffer sql = new StringBuffer("select * from users where 1=1");
		sql.append(" and userid=").append(userId);
		// sql.append(" order by type ");
		RowMapper<Users> rowMapper = new BeanPropertyRowMapper<Users>(Users.class);
		List<Users> list = jdbcTemplate.query(sql.toString(), rowMapper);
		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public Users saveUser(Users user) {
		if (user.getUserId() != null) {
			getSession().update(user);
			getSession().flush();
		} else {
			getSession().save(user);
			getSession().flush();
		}
		return user;
	}

	@Override
	public int updateUserPwd(Users user) {
		return 0;
	}

	@Override
	public int updateUserScore(Users user) {
		return 0;
	}

	@Override
	public void deleteUser(int userid) {
		jdbcTemplate.execute("delete from users where 1=1 and userid=" + userid);
	}

	@Override
	public List<Users> queryUsers() {
		String sql = "select * from users where 1=1 order by status ";
		RowMapper<Users> rowMapper = new BeanPropertyRowMapper<Users>(Users.class);
		return jdbcTemplate.query(sql, rowMapper);
	}

	@Override
	public List<Users> queryUsers(Users user) {
		StringBuffer sql = new StringBuffer("select * from users where 1=1");
		if (user.getUserId() != null) {
			sql.append(" and userid=").append(user.getUserId());
		}
		if (StringUtils.isNotEmpty(user.getAccount())) {
			sql.append(" and account='").append(user.getAccount()).append("'");
		}
		RowMapper<Users> rowMapper = new BeanPropertyRowMapper<Users>(Users.class);
		return jdbcTemplate.query(sql.toString(), rowMapper);
	}
}