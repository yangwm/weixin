package com.yangwm.wx.web.util;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;


/**
 * HttpClient管理类
 * @author yangwm
 * @date 2015-07-14
 */
public class HttpClientMgr {
	
	static HttpClient httpclient = null;
	static List<Cookie> list = new ArrayList<Cookie>();
	
	private static final String CHARSET = HTTP.UTF_8;
	
	private HttpClientMgr(){  
		  
    }  
	
	public static synchronized HttpClient getSaveHttpClient(){  
        if(httpclient == null){  
            HttpParams params = new BasicHttpParams();  
            //设置基本参数  
//            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);  
            HttpProtocolParams.setContentCharset(params, CHARSET);  
            HttpProtocolParams.setUseExpectContinue(params, true);  
            /*连接超时*/  
            HttpConnectionParams.setConnectionTimeout(params, 2000);  
            /*请求超时*/  
            HttpConnectionParams.setSoTimeout(params, 4000);  
            //设置HttpClient支持HTTp和HTTPS两种模式  
            SchemeRegistry schReg = new SchemeRegistry();  
            schReg.register(new Scheme("http",80, PlainSocketFactory.getSocketFactory()));  
            schReg.register(new Scheme("https",443, SSLSocketFactory.getSocketFactory()));  
            //使用线程安全的连接管理来创建HttpClient  
            ClientConnectionManager conMgr = new ThreadSafeClientConnManager(params, schReg);  
            httpclient = new DefaultHttpClient(conMgr, params);  
        }  
        return httpclient;  
    }
	
}
