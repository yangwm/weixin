package com.yangwm.wx.web.util;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;

/**
 * 处理httpclient访问的操作
 * @author yangwm
 * @date 2015-07-14
 */
public class HttpConnHandler {
	
	/**
	 * 处理http的get请求
	 * 
	 * @param url
	 * @return 返回请求的网站
	 */
	public static String opGetHandler(String url){
		return opGetHandler(url,null);
	}
	
	/**
	 * 处理http的post请求
	 * 
	 * @param url 请求的url
	 * @param params 需要提交的参数
	 * @return 返回请求的网站
	 */
	public static String opPostHandler(String url, List<NameValuePair> params){
		return opPostHandler(url,params,null);
	}
	
	/**
	 * 处理http的get请求
	 * 
	 * @param url
	 * @param header
	 * @return
	 */
	public static String opGetHandler(String url, HttpGet paramHttpGet){
		HttpClient httpclient = HttpClientMgr.getSaveHttpClient();
		String html = "";
		if(url!=null && !"".equals(url) && (url.indexOf("http://")!=-1 || url.indexOf("https://")!=-1)){
			HttpGet httpget = null;
			try {
				if(paramHttpGet == null){
					httpget = new HttpGet(url);
				}else{
					httpget = paramHttpGet;
				}
				HttpResponse response;
				response = httpclient.execute(httpget);
				HttpEntity entity = response.getEntity();
				entity = response.getEntity();
				html = EntityUtils.toString(entity, "UTF-8");
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				httpget.releaseConnection();
			}
		}
		return html;
	}
	
	/**
	 * 处理http的post请求
	 * 
	 * @param url 请求的url
	 * @param params 需要提交的参数
	 * @return 返回请求的网站
	 */
	public static String opPostHandler(String url, List<NameValuePair> params,HttpPost paramHttpPost){
		HttpClient httpclient = HttpClientMgr.getSaveHttpClient();
		String html = "";
		if(url!=null && !"".equals(url) && (url.indexOf("http://")!=-1 || url.indexOf("https://")!=-1) && params!=null){
			HttpPost httppost = null;
			try {
				if(paramHttpPost == null){
					httppost = new HttpPost(url);
				}else{
					httppost = paramHttpPost;
				}
	        	httppost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();
				html = EntityUtils.toString(entity, "UTF-8");
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				//释放连接
		        httppost.releaseConnection();
			}
		}
		return html;
	}
}
