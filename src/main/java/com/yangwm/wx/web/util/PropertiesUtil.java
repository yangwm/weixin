package com.yangwm.wx.web.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtil{
	private String resName="sysconfig.properties";
	private Properties properties;

	private PropertiesUtil(){
		InputStream is=PropertiesUtil.class.getClassLoader().getResourceAsStream(resName);
		properties=new Properties();
		try{
			properties.load(is);
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	public static PropertiesUtil getInstance(){
		return PropertiesUtilHolder.instance;
	}

	private static class PropertiesUtilHolder{
		private static final PropertiesUtil instance=new PropertiesUtil();
	}

	public String getProperty(String key){
		return properties.getProperty(key);
	}
	
	public String getProperty(String key, String defaultValue){
		return properties.getProperty(key, defaultValue);
	}
	
	public int getIntProperty(String key){
		return Integer.parseInt(properties.getProperty(key));
	}
	
	public int getIntProperty(String key,String defaultValue){
		return Integer.parseInt(properties.getProperty(key,defaultValue));
	}
	
	public long getLongProperty(String key){
		return Long.parseLong(properties.getProperty(key));
	}
	
	public boolean getBooleanProperty(String key){
		return Boolean.parseBoolean(properties.getProperty(key));
	}
	
	public String getSsoUrl(){
		return properties.getProperty("sso.url");
	}
	
	public boolean isSsoLoginStrategy(){
		if("SSO".equals(properties.getProperty("login.strategy"))){
			return true;
		}
		return false;
	}
	
	public boolean isFormLoginStrategy(){
		if("FORM".equals(properties.getProperty("login.strategy"))){
			return true;
		}
		return false;
	}
}