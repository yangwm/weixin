package com.yangwm.wx.web.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class WeixinUtils {
	private static final Logger logger = LogManager.getLogger(WeixinUtils.class);
	
	private static int TIMEOUT=30000;
	private static String ACCESS_TOKEN = null;
	private static final String APP_ID = "wx2e34127289eb3075";
	private static final String APP_SECRET = "6804898be2d216cfc94f58131617fa85";
	private static final String ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={0}&secret={1}";
	private static final String QRCODE_TICKET_URL = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token={0}";
	private static final String QRCODE_SRC_URL = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket={0}";
	private static final String STEND_TEMPLATE_URL = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={0}";
	private static final String WEB_AUTH_URL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={0}&redirect_uri={1}&response_type=code&scope=snsapi_base&state={2}#wechat_redirect";
	private static final String WEB_AUTH_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid={0}&secret={1}&code={2}&grant_type=authorization_code";
	public static final String CHAR_SET = "UTF-8";

	public static String getSignature(String token, String timestamp, String nonce) {
		String[] array = new String[] { token, timestamp, nonce };
		Arrays.sort(array);
		StringBuffer sb = new StringBuffer();
		for (String str : array) {
			sb.append(str);
		}
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update(sb.toString().getBytes());
			byte[] digest = md.digest();
			StringBuffer hexStr = new StringBuffer();
			String shaHex = "";
			for (int i = 0; i < digest.length; i++) {
				shaHex = Integer.toHexString(digest[i] & 0xFF);
				if (shaHex.length() < 2) {
					hexStr.append(0);
				}
				hexStr.append(shaHex);
			}
			return hexStr.toString();

		} catch (NoSuchAlgorithmException e) {
			logger.error("获取签名信息失败", e.getCause());
		}
		return "";
	}

	public static String getAccessToken() {
		if (ACCESS_TOKEN != null) {
			logger.debug("从内存中获取到AccessToken:{}", ACCESS_TOKEN);
			return ACCESS_TOKEN;
		}
		String access_token_url = MessageFormat.format(ACCESS_TOKEN_URL, APP_ID, APP_SECRET);
		logger.debug("access_token_url转换后的访问地址:{}", access_token_url);
		try {
			String rtn = HttpClientUtil.httpGet(access_token_url, TIMEOUT);
			logger.debug(rtn);
			JSONObject obj = JSONObject.parseObject(rtn);
			String accessToken = obj.getString("access_token");
			return accessToken;
		} catch (IOException e) {
			logger.error("获取accessToken出现错误", e.getCause());
		}
		return null;
	}
	
	public static String getAccessToken_bak() {
		if (ACCESS_TOKEN != null) {
			logger.debug("从内存中获取到AccessToken:{}", ACCESS_TOKEN);
			return ACCESS_TOKEN;
		}
		String access_token_url = MessageFormat.format(ACCESS_TOKEN_URL, APP_ID, APP_SECRET);
		logger.debug("access_token_url转换后的访问地址:{}", access_token_url);
		Request request = new Request.Builder().url(access_token_url).build();
		OkHttpClient httpClient = new OkHttpClient();
		Call call = httpClient.newCall(request);
		try {
			Response response = call.execute();
			String resBody = response.body().string();
			logger.debug("获取到相应正文:{}", resBody);
			JSONObject jo = JSONObject.parseObject(resBody);
			String accessToken = jo.getString("access_token");
			String errCode = jo.getString("errcode");
			if (StringUtils.isBlank(errCode)) {
				errCode = "0";
			}
			if ("0".equals(errCode)) {
				logger.debug("获取accessToken成功,值为：{}", accessToken);
				ACCESS_TOKEN = accessToken;
			}
			
			return accessToken;
		} catch (IOException e) {
			logger.error("获取accessToken出现错误", e.getCause());
		}
		return null;
	}

	/**
	 * @see 官网文档：https://developers.weixin.qq.com/doc/offiaccount/Account_Management/Generating_a_Parametric_QR_Code.html
	 * @param accessToken
	 * @param qrCodeType
	 * @param qrCodeValue
	 * @return
	 */
	public static String getQrCodeTiket(String accessToken, String qrCodeType, String qrCodeValue) {
		try {
			String qrcode_ticket_url = MessageFormat.format(QRCODE_TICKET_URL, accessToken);
			logger.debug("qrcode_ticket_url:{}", qrcode_ticket_url);
			
			Map<String, Object>params=new HashMap<>();
			params.put("expire_seconds", 604800);
			params.put("action_name", "QR_STR_SCENE");
			Map<String, Object> sence = new HashMap<>();
			Map<String, Object> scene_str = new HashMap<>();
			scene_str.put("scene_str", MessageFormat.format("{0}#{1}", qrCodeType, qrCodeValue));
			sence.put("scene", scene_str);
			params.put("action_info", sence);
			String jsonStr=JSON.toJSONString(params);
			System.out.println(jsonStr);
			String rtn = HttpClientUtil.httpPostJson(qrcode_ticket_url, jsonStr, TIMEOUT);
			logger.debug(rtn);
			JSONObject obj = JSONObject.parseObject(rtn);
			String qrTicket = obj.getString("ticket");
			/*String errCode = obj.getString("errcode");
			if (StringUtils.isBlank(errCode) || "0".equals(obj.getString(errCode))) {
				logger.debug("获取QrCodeTicket成功,值为：{}", qrTicket);
			}*/
			logger.debug("QrCodeTicket：{}", qrTicket);
			return qrTicket;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getQrCodeTiket_bak(String accessToken, String qeCodeType, String qrCodeValue) {
		String qrcode_ticket_url = MessageFormat.format(QRCODE_TICKET_URL, accessToken);
		logger.debug("qrcode_ticket_url转换后的访问地址:{}", qrcode_ticket_url);
		
		JSONObject pd = new JSONObject();
		pd.put("expire_seconds", 604800);
		pd.put("action_name", "QR_STR_SCENE");
		JSONObject sence = new JSONObject();
		sence.put("scene", JSONObject.parseObject("{\"scene_str\":\"" + MessageFormat.format("{0}#{1}", qeCodeType, qrCodeValue) + "\"}"));
		pd.put("action_info", sence);
		logger.debug("提交内容{}", pd.toJSONString());
		MediaType JSON = MediaType.parse("application/json;charset=utf-8");
		RequestBody body = RequestBody.create(JSON, pd.toJSONString());
		
		Request request = new Request.Builder().url(qrcode_ticket_url).post(body).build();
		OkHttpClient httpClient = new OkHttpClient();
		Call call = httpClient.newCall(request);
		try {
			Response response = call.execute();
			String resBody = response.body().string();
			logger.debug("获取到相应正文:{}", resBody);
			JSONObject jo = JSONObject.parseObject(resBody);
			String qrTicket = jo.getString("ticket");
			String errCode = jo.getString("errcode");
			if (StringUtils.isBlank(errCode)) {
				errCode = "0";
			}
			if ("0".equals(jo.getString(errCode))) {
				logger.debug("获取QrCodeTicket成功,值为：{}", qrTicket);
			}
			return qrTicket;
		} catch (IOException e) {
			logger.error("获取QrCodeTicket出现错误", e.getCause());
		}
		return null;
	}

	/**
	 * @see 官网文档：https://developers.weixin.qq.com/doc/offiaccount/Account_Management/Generating_a_Parametric_QR_Code.html
	 * @param qrCodeTicket
	 * @return
	 */
	public static InputStream getQrCodeStream(String qrCodeTicket) {
		String qrcode_src_url = MessageFormat.format(QRCODE_SRC_URL, qrCodeTicket);
		logger.debug("qrcode_src_url:{}", qrcode_src_url);
		try {
			URL url = new URL(qrcode_src_url);
			return url.openStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @see 官网文档：https://developers.weixin.qq.com/doc/offiaccount/Account_Management/Generating_a_Parametric_QR_Code.html
	 * @param qrCodeTicket
	 * @return
	 */
	public static InputStream getQrCodeStream_bak(String qrCodeTicket) {
		String qrcode_src_url = MessageFormat.format(QRCODE_SRC_URL, qrCodeTicket);
		logger.debug("qrcode_src_url:{}", qrcode_src_url);
		Request request = new Request.Builder().url(qrcode_src_url).get().build();
		OkHttpClient httpClient = new OkHttpClient();
		Call call = httpClient.newCall(request);
		try {
			Response response = call.execute();
			return response.body().byteStream();
		} catch (IOException e) {
			logger.error("获取qrcode_src_url出现错误", e.getCause());
		}
		return null;
	}

	public static void sendTempalteMsg(String accessToken, String openId, String templateId, String url, Map data) {
		String sendTemplate_url = MessageFormat.format(STEND_TEMPLATE_URL, accessToken);
		logger.debug("sendTemplate_url 转换后的访问地址:{}", sendTemplate_url);
		JSONObject jo = new JSONObject();
		jo.put("touser", openId);
		jo.put("template_id", templateId);
		jo.put("url", url);
		jo.put("data", data);
		logger.debug("提交内容{}", jo.toJSONString());
		MediaType JSON = MediaType.parse("application/json;charset=utf-8");
		RequestBody body = RequestBody.create(JSON, jo.toJSONString());
		Request request = new Request.Builder().url(sendTemplate_url).post(body).build();
		OkHttpClient client = new OkHttpClient();
		try (Response response = client.newCall(request).execute()) {
			String resBody = response.body().string();
			logger.debug("获取到相应正文:{}", resBody);

		} catch (IOException e) {
			logger.error("发送模板消息出现错误", e.getCause());
		}
	}

	public static String InPutstream2String(InputStream aStream, String charSet) throws IOException {
		try {
			byte[] fBuffer = new byte[8192];
			ByteArrayOutputStream ftemp = new ByteArrayOutputStream(8192);
			int flen;
			do {
				flen = aStream.read(fBuffer, 0, fBuffer.length);
				if (flen > 0)
					ftemp.write(fBuffer, 0, flen);
			} while (flen >= 0);
			return new String(ftemp.toByteArray(), charSet);
		} finally {
			aStream.close();
		}
	}

	public static BufferedImage buildWebAuthUrlQrCode(String redirectUrl, String state) {
		logger.debug("redirectUrl :{},state:{}", redirectUrl, state);
		try {
			redirectUrl = URLEncoder.encode(redirectUrl, CHAR_SET);
			state = URLEncoder.encode(state, CHAR_SET);

			String web_auth_url = MessageFormat.format(WEB_AUTH_URL, APP_ID, redirectUrl, state);
			logger.debug("web_auth_url:{}", web_auth_url);
			QRCodeWriter qrCodeWriter = new QRCodeWriter();
			BitMatrix bitMatrix = qrCodeWriter.encode(web_auth_url, BarcodeFormat.QR_CODE, 500, 500);
			return MatrixToImageWriter.toBufferedImage(bitMatrix);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static JSONObject getWebAuthTokenInfo(String code) {
		String web_auth_token_url = MessageFormat.format(WEB_AUTH_TOKEN_URL, APP_ID, APP_SECRET, code);
		logger.debug("web_auth_token_url:{}", web_auth_token_url);
		try {
			String rtn = HttpClientUtil.httpGet(web_auth_token_url, TIMEOUT);
			logger.debug(rtn);
			return JSON.parseObject(rtn);
		} catch (IOException e) {
			logger.error("WebAuthTokenInfo", e.getCause());
			e.printStackTrace();
		}
		return null;
	}
	
	public static JSONObject getWebAuthTokenInfo_bak(String code) {
		String web_auth_token_url = MessageFormat.format(WEB_AUTH_TOKEN_URL, APP_ID, APP_SECRET, code);
		logger.debug("web_auth_token_url:{}", web_auth_token_url);
		Request request = new Request.Builder().url(web_auth_token_url).build();
		OkHttpClient client = new OkHttpClient();
		try (Response response = client.newCall(request).execute()) {
			String resBody = response.body().string();
			logger.debug(resBody);
			return JSONObject.parseObject(resBody);
		} catch (IOException e) {
			logger.error("获取webauthtoken出现异常", e.getCause());
		}
		return null;
	}
}
