package com.yangwm.wx.web.util;

import org.apache.commons.lang.StringUtils;

public class WxTemplate {

	/** task id */
	private String id;
	private String name;
	private String department;
	private String owner;
	private String position;
	
	@Override
	public String toString() {
		StringBuffer buf=new StringBuffer();
		buf.append("1、任务ID：\t").append(StringUtils.isEmpty(id)?"":id).append("\n")
		   .append("2、任务名称：\t").append(StringUtils.isEmpty(name)?"":(name.length()>500?name.substring(0,500):name)).append("\n")
		   .append("3、发起人：\t").append(StringUtils.isEmpty(owner)?"":owner).append("\n")
		   .append("4、所属部门：\t").append(StringUtils.isEmpty(department)?"":department).append("\n")
		   .append("5、所在位置：\t").append(StringUtils.isEmpty(position)?"":position).append("\n")
		;
		return buf.toString();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
}