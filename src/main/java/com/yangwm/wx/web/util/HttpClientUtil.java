package com.yangwm.wx.web.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * HttpClient4工具类
 * @author yangwm
 */
public class HttpClientUtil {

	private static Logger logger = LoggerFactory.getLogger(HttpClientUtil.class.getName());
	
	/**
	 * post json数据
	 * @param url
	 * @param jsonStr
	 * @param timeout
	 * @return
	 * @throws IOException 
	 */
	public static String httpPostJson(String url, String jsonStr, int timeout) throws IOException {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = null;

		try {
			httpPost = new HttpPost(url);
			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(timeout).setConnectTimeout(timeout).build();
			httpPost.setConfig(requestConfig);
			StringEntity stringEntity = new StringEntity(jsonStr, ContentType.APPLICATION_JSON);//发送json数据需要设置contentType
			stringEntity.setContentEncoding("utf8");
			//当以application/x-www-form-urlencoded的方式传送数据。请求的内容需要以..=..&..=..的格式提交，在请求体内内容将会以”&”和“ = ”进行拆分。
			//stringEntity.setContentType("application/x-www-form-urlencoded");
			httpPost.setEntity(stringEntity);
			CloseableHttpResponse response = httpclient.execute(httpPost);
			return EntityUtils.toString(response.getEntity());

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("httpPostJson error "+e.getMessage(), e);
		}finally {
			httpclient.close();
		}
		return null;
	}
	
	public static String httpPostJson2(String url, String jsonStr, int timeout) throws IOException {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = null;
		
		try {
			httpPost = new HttpPost(url);
			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(timeout).setConnectTimeout(timeout).build();
			httpPost.setConfig(requestConfig);
			StringEntity stringEntity = new StringEntity(jsonStr, ContentType.APPLICATION_JSON);//发送json数据需要设置contentType
			stringEntity.setContentEncoding("utf8");
			//当以application/x-www-form-urlencoded的方式传送数据。请求的内容需要以..=..&..=..的格式提交，在请求体内内容将会以”&”和“ = ”进行拆分。
			stringEntity.setContentType("application/x-www-form-urlencoded");
			httpPost.setEntity(stringEntity);
			CloseableHttpResponse response = httpclient.execute(httpPost);
			return EntityUtils.toString(response.getEntity());
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("httpPostJson error "+e.getMessage(), e);
		}finally {
			httpclient.close();
		}
		return null;
	}
	
	/**
	 * post请求
	 * @param url
	 * @param params
	 * @param timeout
	 * @return
	 * @throws IOException 
	 */
	public static String httpPost(String url,Map<String,String> params, int timeout) throws IOException {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = null;
		
		try {
			httpPost = new HttpPost(url);
			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(timeout).setConnectTimeout(timeout).build();
			httpPost.setConfig(requestConfig);
			List <NameValuePair> nvps = new ArrayList <NameValuePair>();
			if(params!=null&&params.size()>0){
				Set<String> keys=params.keySet();
				for (String key : keys) {
					String value = params.get(key);
					nvps.add(new BasicNameValuePair(key, value));
				}
			}
			httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
			HttpResponse response = httpclient.execute(httpPost);
			return EntityUtils.toString(response.getEntity(), "UTF-8");
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("httpPost error "+e.getMessage(), e);
		}finally {
			httpclient.close();
		}
		return null;
	}
	/**
	 * get请求
	 * @param url
	 * @param params
	 * @param timeout
	 * @return
	 * @throws IOException 
	 */
	public static String httpGet(String url, int timeout) throws IOException {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = null;
		
		try {
			httpGet = new HttpGet(url);
			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(timeout).setConnectTimeout(timeout).build();
			httpGet.setConfig(requestConfig);
			HttpResponse response = httpclient.execute(httpGet);
			return EntityUtils.toString(response.getEntity(), "UTF-8");
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("httpGet error "+e.getMessage(), e);
		}finally {
			httpclient.close();
		}
		return null;
	}
}
