package com.yangwm.wx.web.base;

public class Constant {

	// session访问
	public static String session_visitsource = "session_visitsource";

	public static String visitsource_weixin = "visitsource_weixin";

	public static String session_sysusermap = "session_sysusermap";

	public static String session_user = "session_user"; // 登陆session

	public static String img_bdsplit = "ueditor/jsp/upload/image/";

	public static String sql_like_split = "@like#";

	public static String compare_begin = "comparebegin";

	public static String compare_end = "compareend";

	public static String compare_equal_begin = "compareequalbegin";

	public static String compare_equal_end = "compareequalend";

	public static String rand_in_str_list = "randinstrlist";// 此功能只能对整形数据有效，对于字符串等其他类型还代考虑

	public static String authority_list = "authority_list@noclear"; // 权限列表，缓存表，不会被清空

	public static String cachenoclear = "@noclear"; // 如果缓存加上此标志，则不会在缓存过多时被清除

	public static String cachenocleartablestr = ""; // 不会被清除缓存的表

	public static String cachesqlquery = "@cachesqlquery";

	public static String md5key = "yhcfmd5key";

	public static String rsasplit = "@@rsasplit@@";

	/** 默认分页条数 */
	public static int PAGE_SIZE = 10;
	
	/** sys_code类型：分割符 */
	public static String SYS_CODE_TYPE_DELIMITER = "###";
	
	/** sys_code类型：报修原因 */
	public static String SYS_CODE_TYPE_REASON = "Reason";
	/** sys_code类型：部门 */
	public static String SYS_CODE_TYPE_DEPT = "DEPT";
	/** sys_code类型：电子大屏 */
	public static String SYS_CODE_TYPE_ELECTRONIC_SCREEN = "ElectronicScreen";
	/** sys_code类型：设备类型 */
	public static String SYS_CODE_TYPE_DEVICE_TYPE = "DeviceType";
	/** sys_code类型：任务类型 */
	public static String SYS_CODE_TYPE_TASK_TYPE = "TaskType";
	/** sys_code类型：任务优先级 */
	public static String SYS_CODE_TYPE_TASK_PRIORITY = "TaskPriority";
	/** sys_code类型：host */
	public static String SYS_CODE_TYPE_HOST = "HOST";
	/** sys_code类型：QRCODE-PATH */
	public static String SYS_CODE_TYPE_QRCODE_PATH = "QrCodePath";
	/** sys_code类型：Attachment-PATH */
	public static String SYS_CODE_TYPE_ATTACHMETN_PATH = "AttachmentPath";
	
	/** 省市区三级联动：json格式的代码 */
	public static String CONFIG_PROVINCE_CITY = "city.min.json";
	
	/** 登录session类型：app */
	public static String LOGIN_SESSION_APP = "##APP##";
	/** 登录session类型：web */
	public static String LOGIN_SESSION_WEB = "##WEB##";
	
	/** configuration类型：行业 */
	public static String CONFIG_SECTOR = "SECTOR";
	/** configuration类型：年份 */
	public static String CONFIG_YEAR = "YEAR";
	/** configuration类型：政治面貌 */
	public static String CONFIG_POLITICS_STATUS = "POLITICS_STATUS";
	/** configuration类型：家属关系 */
	public static String CONFIG_FAMILY_RELATIONSHIP = "FAMILY_RELATIONSHIP";
	/** configuration类型：是否为户主 */
	public static String CONFIG_IS_HOUSEHOLDER = "IS_HOUSEHOLDER";
	/** configuration类型：性别 */
	public static String CONFIG_SEX = "SEX";
	/** configuration类型：户口类型 */
	public static String CONFIG_HOUSEHOLDER_TYPE = "HOUSEHOLDER_TYPE";
	/** configuration类型：户口迁移类型 */
	public static String CONFIG_MIGRATION_TYPE = "MIGRATION_TYPE";
	/** configuration类型：审核状态 */
	public static String CONFIG_AUDIT_STATUS = "AUDIT_STATUS";
	/** configuration类型：产业类型 */
	public static String CONFIG_INDUSTRY_TYPE = "INDUSTRY_TYPE";
	/** configuration类型：单位性质 */
	public static String CONFIG_COMPANY_TYPE = "COMPANY_TYPE";
	/** configuration类型：教育类型 */
	public static String CONFIG_EDUCATION_TYPE = "EDUCATION_TYPE";
	/** configuration类型：牲畜类型 */
	public static String CONFIG_LIVESTOCK_TYPE = "LIVESTOCK_TYPE";
	/** configuration类型：车辆类型 */
	public static String CONFIG_VEHICLE_TYPE = "VEHICLE_TYPE";
	/** configuration类型：农机类型 */
	public static String CONFIG_FARM_MACHINERY_TYPE = "FARM_MACHINERY_TYPE";
	/** configuration类型：婚姻状态 */
	public static String CONFIG_MARITAL_TYPE = "MARITAL_TYPE";
	/** configuration类型：房屋结构 */
	public static String CONFIG_BUILDING_STRUCTURE = "BUILDING_STRUCTURE";
	/** configuration类型：土地类型 */
	public static String CONFIG_LAND_TYPE = "LAND_TYPE";
	/** configuration类型：所属村落 */
	public static String CONFIG_VILLAGE_LIST = "VILLAGE_LIST";
	/** configuration类型：医保类型 */
	public static String CONFIG_HEALTH_CARE_TYPE = "HEALTH_CARE_TYPE";
	/** configuration类型：社保类型 */
	public static String CONFIG_SOCIAL_SECURITY_TYPE = "SOCIAL_SECURITY_TYPE";
}