package com.yangwm.wx.web.base;

import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;

import java.util.Locale;
import java.util.Map;

/**
 * 多视图支持
 *
 * @author denglp
 * @since 2013-03-11
 */
public class MixedViewResolver implements ViewResolver {

    private Map<String, ViewResolver> resolvers;

    public void setResolvers(Map<String, ViewResolver> resolvers) {
        this.resolvers = resolvers;
    }

    @Override
    public View resolveViewName(String viewName, Locale locale) throws Exception {
        int i = viewName.lastIndexOf(".");
        if (i > -1) {
            //扩展名
            String suffix = viewName.substring(i + 1);
            suffix = suffix.toLowerCase();
            ViewResolver viewResolver = resolvers.get(suffix);
            if (viewResolver == null) {
                throw new Exception("No ViewResolver for " + suffix);
            }
            return viewResolver.resolveViewName(viewName, locale);
        }
        //默认返jsp视图
        ViewResolver viewResolver = resolvers.get("jsp");
        return viewResolver.resolveViewName(viewName, locale);
    }

}
