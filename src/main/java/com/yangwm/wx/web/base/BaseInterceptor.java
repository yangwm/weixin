package com.yangwm.wx.web.base;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * 基础拦截器
 *
 * @author denglp
 * @since 2013-03-19
 */
public class BaseInterceptor extends HandlerInterceptorAdapter {

    /**
     * 在业务方法处理前调用
     */
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        BaseController baseController = (BaseController) handler;
        baseController.setResponse(response);
        return true;
    }

    /**
     * 在业务方法处理后调用。
     */
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
            throws Exception {
        
    	if (modelAndView != null) {
            ModelMap modelMap = modelAndView.getModelMap();
            modelMap.put("contextPath", WebContextUtil.getContextPath());//设置项目虚拟路径
        }
    }
}
