package com.yangwm.wx.web.base;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * base Controller
 * @author denglp
 * @since 2013-01-24
 */
public class BaseController {
    private HttpServletResponse response;
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 获取HttpServletRequest对象
     * @return HttpServletRequest
     */
    public HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

	/**
	 * 获取HttpServletResponse对象
	 * @return HttpServletResponse
	 */
	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}
    
	/** 获取当前应用 */
	public String getRootPath(HttpServletRequest request) {
		String path = request.getSession().getServletContext().getRealPath("/");
		return path;
	}

	/** 判断是否为微信端 */
	public boolean checkWeiXin(HttpServletRequest request) {
		String userAgent = "";
		String userAgents = request.getHeader("user-agent");
		String reqtype = request.getParameter("reqtype");

		if (reqtype == null) {
			reqtype = this.getSessionStr(request, "reqtype");
			if (reqtype == null) {
				reqtype = "";
			}
		} else {
			this.putSession(request, "reqtype", reqtype);
		}

		if (userAgents != null) {
			userAgent = userAgents;
			userAgent = userAgent.toUpperCase();
		}

		if ((userAgent.toLowerCase()).indexOf("micromessenger") > -1) {
			return true;
		} else {
			return false;
		}
	}

	/** 判断是否为移动端 */
	public boolean checkMobile(HttpServletRequest request) {
		String userAgent = "";
		String userAgents = request.getHeader("user-agent");
		String reqtype = request.getParameter("reqtype");

		if (reqtype == null) {
			reqtype = this.getSessionStr(request, "reqtype");
			if (reqtype == null) {
				reqtype = "";
			}
		} else {
			this.putSession(request, "reqtype", reqtype);
		}

		if (userAgents != null) {
			userAgent = userAgents;
			userAgent = userAgent.toUpperCase();
		}

		if ((userAgent.toLowerCase()).indexOf("micromessenger") > -1) {
			this.putSession(request, Constant.session_visitsource, Constant.visitsource_weixin);
		}

		if (userAgent.indexOf("NOKI") > -1 || // Nokia phones and emulators
				userAgent.indexOf("ERIC") > -1 || // Ericsson WAP phones and
				// emulators
				userAgent.indexOf("WAPI") > -1 || // Ericsson WapIDE 2.0
				userAgent.indexOf("MC21") > -1 || // Ericsson MC218
				userAgent.indexOf("AUR") > -1 || // Ericsson R320
				userAgent.indexOf("R380") > -1 || // Ericsson R380
				userAgent.indexOf("UP.B") > -1 || // UP.Browser
				userAgent.indexOf("WINW") > -1 || // WinWAP browser
				userAgent.indexOf("UPG1") > -1 || // UP.SDK 4.0
				userAgent.indexOf("UPSI") > -1 || // another kind of
				// UP.Browser
				userAgent.indexOf("QWAP") > -1 || // unknown QWAPPER browser
				userAgent.indexOf("JIGS") > -1 || // unknown JigSaw browser
				userAgent.indexOf("JAVA") > -1 || // unknown Java based
				// browser
				userAgent.indexOf("ALCA") > -1 || // unknown Alcatel-BE3
				// browser (UP based)
				userAgent.indexOf("MITS") > -1 || // unknown Mitsubishi
				// browser
				userAgent.indexOf("MOT-") > -1 || // unknown browser (UP
				// based)
				userAgent.indexOf("MY S") > -1 || // unknown Ericsson devkit
				// browser
				userAgent.indexOf("WAPJ") > -1 || // Virtual WAPJAG
				// www.wapjag.de
				userAgent.indexOf("FETC") > -1 || // fetchpage.cgi Perl
				// script from
				// www.wapcab.de
				userAgent.indexOf("ALAV") > -1 || // yet another unknown UP
				// based browser
				userAgent.indexOf("WAPA") > -1 || // another unknown browser
				// (Web based
				// "Wapalyzer")
				userAgent.indexOf("OPER") > -1 || // Opera

				userAgent.indexOf("DOPOD") > -1 || // 多普达

				userAgent.indexOf("SYMBIAN") > -1 || // symbian系统

				userAgent.indexOf("ANDROID") > -1 || // 安卓系统

				userAgent.indexOf("IPHONE") > -1 || // IOS系统

				userAgent.indexOf("MOBILE") > -1 // 移动端系统
		) {
			return true;
		} else {
			if (reqtype.equals("mobile")) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	public boolean checkEmpty(String str) {
		boolean result = false;
		if (str == null || str.equals("null") || str.equals("")) {
			result = true;
		}
		return result;
	}

	/**** 判断是否为IOS端 */
	public boolean checkIOS(HttpServletRequest request) {
		String userAgent = "";
		String userAgents = request.getHeader("user-agent");
		String reqtype = request.getParameter("reqtype");
		if (userAgents != null) {
			userAgent = userAgents;
			userAgent = userAgent.toUpperCase();
		}
		if (userAgent.indexOf("IPHONE") > -1) {
			return true;
		} else {
			return false;
		}
	}

	// 放入session
	public void putSession(HttpServletRequest request, String paramname, Object valuestr) {
		HttpSession session = request.getSession();
		// session.putValue(paramname, valuestr);
		session.setAttribute(paramname, valuestr);
	}

	// 移开session
	public void removeSession(HttpServletRequest request, String paramname) {
		HttpSession session = request.getSession();
		session.removeAttribute(paramname);

	}

	// 获取session内容
	public Object getSession(HttpServletRequest request, String paramname) {
		HttpSession session = request.getSession();
		return session.getAttribute(paramname);
	}

	// 获取session内容
	public String getSessionStr(HttpServletRequest request, String paramname) {
		HttpSession session = request.getSession();
		return String.valueOf(session.getAttribute(paramname));
	}

	// 获取session内容
	public String getParameter(HttpServletRequest request, String paramname) {
		return request.getParameter(paramname);
	}
	
	// 获取session内容
	public String getParameter(HttpServletRequest request, String paramname, String defualvalue) {
		String value = request.getParameter(paramname);
		if(StringUtils.isBlank(value)){
			return defualvalue;
		}
		return value;
	}
	
	// 放入cookie
	public void putCookie(HttpServletResponse response, String paramname, String valuestr) {
		Cookie cookie = new Cookie(paramname, valuestr);
		cookie.setMaxAge(14 * 24 * 60 * 60);
		cookie.setPath("/");
		response.addCookie(cookie);
	}

	// 获取cookie
	public String getCookie(HttpServletRequest request, String paramname) {
		Cookie[] cookies = request.getCookies();
		String result = "";
		if (cookies == null) {
		} else {
			for (int i = 0; i < cookies.length; i++) {
				Cookie cookie = cookies[i];
				String cookiename = cookie.getName();
				if (cookiename.equals(paramname)) {
					result = cookie.getValue();
				}
			}
		}
		return result;
	}

	// 移除cookie
	public void removeCookie(HttpServletResponse response, HttpServletRequest request, String paramname) {
		Cookie[] cookies = request.getCookies();
		String result = "";
		if (cookies == null) {
		} else {
			for (int i = 0; i < cookies.length; i++) {
				Cookie cookie = cookies[i];
				String cookiename = cookie.getName();
				if (cookiename.equals(paramname)) {
					Cookie delcookie = new Cookie(paramname, "0");// 这边得用"",不能用null
					delcookie.setMaxAge(-1);
					delcookie.setPath("/");
					response.addCookie(delcookie);
				}
			}
		}
	}

	/**
	 * @see 打印异常
	 * @author jacklen.xie
	 */
	public static String getTrace(Throwable t) {
		StringWriter stringWriter = new StringWriter();
		PrintWriter writer = new PrintWriter(stringWriter);
		t.printStackTrace(writer);
		StringBuffer buffer = stringWriter.getBuffer();
		return buffer.toString();
	}

	// 放入页面参数
	public void setParamter(HttpServletRequest request, String parakey,
			Object object) {
		request.setAttribute(parakey, object);
	}

	public Map getRequestMap(HttpServletRequest request) throws Exception {
		Map paramap = new HashMap();
		Map map = request.getParameterMap();
		StringBuffer result = new StringBuffer("");
		if (map == null || map.size() == 0) {
		} else {
			Set<String> keyset = map.keySet();
			for (Iterator it = keyset.iterator(); it.hasNext();) {
				String keys = String.valueOf(it.next());
				String str = String.valueOf(request.getParameter(keys));
				paramap.put(keys, str);
			}
		}
		return paramap;
	}

	/**
	 * 获取访问者IP
	 * 在一般情况下使用Request.getRemoteAddr()即可，但是经过nginx等反向代理软件后，这个方法会失效。
	 * 本方法先从Header中获取X-Real-IP，如果不存在再从X-Forwarded-For获得第一个IP(用,分割)，
	 * 如果还不存在则调用Request .getRemoteAddr()。
	 * @param request
	 * @return
	 */
	public String getIpAddr(HttpServletRequest request) throws Exception {
		String ip = request.getHeader("X-Real-IP");
		if (!StringUtils.isBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
			return ip;
		}
		ip = request.getHeader("X-Forwarded-For");
		if (!StringUtils.isBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
			// 多次反向代理后会有多个IP值，第一个为真实IP。
			int index = ip.indexOf(',');
			if (index != -1) {
				return ip.substring(0, index);
			} else {
				return ip;
			}
		} else {
			return request.getRemoteAddr();
		}
	}

	/** 
     * 获取当前网络ip 
     * @param request 
     * @return 
     */  
	public String getIpAddr2(HttpServletRequest request) {
		String ipAddress = request.getHeader("x-forwarded-for");
		if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getHeader("Proxy-Client-IP");
		}
		if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getRemoteAddr();
			if (ipAddress.equals("127.0.0.1") || ipAddress.equals("0:0:0:0:0:0:0:1")) {
				// 根据网卡取本机配置的IP
				InetAddress inet = null;
				try {
					inet = InetAddress.getLocalHost();
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}
				ipAddress = inet.getHostAddress();
			}
		}
		// 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
		if (ipAddress != null && ipAddress.length() > 15) { // "***.***.***.***".length() = 15
			if (ipAddress.indexOf(",") > 0) {
				ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
			}
		}
		return ipAddress;
	}
}