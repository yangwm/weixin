package com.yangwm.wx.web.base;

import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.ServletContext;

/**
 * 获取服务器环境
 *
 * @author ping
 * @since 20100326
 */
public class WebContextUtil {

    private static ServletContext servletContext;//上下文
    private static String webPath;//本系统在操作系统的本地路径
    private static String contextPath;//系统虚拟路径

    public static ServletContext getServletContext() {
        return servletContext;
    }

    public static void setServletContext(ServletContext servletContext) {
        WebContextUtil.servletContext = servletContext;
    }

    public static void setWebPath(String webPath) {
        WebContextUtil.webPath = webPath;
    }

    /**
     * 获取服务器部署的路径
     *
     * @return
     */
    public static String getWebPath() {
        if (webPath == null || webPath.equals("")) {
            webPath = servletContext.getRealPath("/");
        }
        return webPath;
    }

    /**
     * 获取服务器部署的contextPath，已去掉前面的斜杠。
     *
     * @return
     */
    public static String getContextPath() {
        if (contextPath == null || contextPath.equals("")) {
            contextPath = servletContext.getContextPath();
        }
        return contextPath;
    }

    /**
     * 获取sessionId
     *
     * @return
     */
    public static String getSessionId() {
        return RequestContextHolder.getRequestAttributes().getSessionId();
    }
}

