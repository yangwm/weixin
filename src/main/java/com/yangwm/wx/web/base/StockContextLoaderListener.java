package com.yangwm.wx.web.base;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.springframework.web.context.ContextLoaderListener;

/**
 * 初始化服务器环境
 *
 * @author denglp
 * @since 2010-05-12
 */
public class StockContextLoaderListener extends ContextLoaderListener {

    public void contextInitialized(ServletContextEvent event) {
        super.contextInitialized(event);
        ServletContext servletContext = event.getServletContext();

        // 设置本地环境
        WebContextUtil.setServletContext(servletContext);
    }
}
