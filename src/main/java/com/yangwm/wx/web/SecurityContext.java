package com.yangwm.wx.web;

import java.io.Serializable;
import java.util.List;

import com.yangwm.wx.web.bean.Role;

/**
 * 登陆安全上下文。在用户通过身份验证之后，系统会创建一份登陆安全上下文，并且保存在{@link SecurityContextHolder}中。
 * 
 * @author yangwm
 */
public interface SecurityContext extends Serializable {
	/**
	 * 当前登陆上下文关联的用户对象是否为系预设的超级统管理员。
	 * 
	 * @return
	 */
	boolean isAdministrator();
	/**
	 * 当前登陆上下文关联的用户对象是否为普通管理员。
	 * 
	 * @return
	 */
	public boolean isAdminNormal();
	/**
	 * 当前登陆上下文关联的用户对象是否为普通用户
	 * 
	 * @return
	 */
	boolean isGeneralUser();
	/**
	 * 得到当前客户代码。通常在支持SaaS架构时使用。
	 * 
	 * @return
	 */
	String getClientCode();
	/**
	 * 设置当前客户代码。通常在支持SaaS架构时使用。请注意，系统应该只是在用户登陆时调用一次这个接口。
	 * 
	 * @param clientCode
	 * @see ClientCodeDataSourceRoutingStrategy#DEFAULT_REQUEST_PARAMETER
	 */
	void setClientCode(String clientCode);
	/**
	 * 得到当前客户userId。通常在支持SaaS架构时使用。
	 * 
	 * @return
	 */
	String getUserId();
	/**
	 * 设置当前客户userId。通常在支持SaaS架构时使用。请注意，系统应该只是在用户登陆时调用一次这个接口。
	 * 
	 * @param userId
	 * @see ClientCodeDataSourceRoutingStrategy#DEFAULT_REQUEST_PARAMETER
	 */
	void setUserId(String userId);
	/**
	 * 是否为后台管理登陆。后台管理允许超级管理员和普通管理员登陆。
	 * 
	 * @return
	 */
	boolean isAdminLogin();
	/**
	 * 设置是否为后台管理登陆。后台管理允许超级管理员和普通管理员登陆。请注意，系统应该只是在用户登陆时调用一次这个接口。
	 * 
	 * @param isAdminLogin
	 */
	void setAdminLogin(boolean isAdminLogin);
	
	void setRoleList(List<Role> list);
	List<Role> getRoleList();
}
